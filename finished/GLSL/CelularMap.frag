uniform vec3 iResolution;
uniform float iGlobalTime;

int nCellWidth  = 20;
int nCellHeight = 20;



vec2 Hash2(vec2 p)
{
    float r = 523.0*sin(dot(p, vec2(53.3158, 43.6143)));
    return vec2(fract(15.32354 * r), fract(17.25865 * r));
}

vec3 map( vec2 uv, int cellWidth, int cellHeight )
{
		vec2 sizeCell = iResolution.xy / vec2( cellWidth, cellHeight ); 
		vec2 currentPos = uv / sizeCell;
		
		currentPos = floor( currentPos );
		
		vec2 currentPoint = uv / sizeCell;
		
		float f1				   = 10000.0;
		float f2                 = 10000.0;
		float f3				   = 10000.0;
		float f4				   = 10000.0;
		float maxDistance = 0.0;
		
		for( int w = -1; w <= 1; w++ )
		{
			for( int h = -1; h <= 1; h++ )
			{
				vec2	pos = currentPos + vec2( w, h );
	
				vec2 randomPoint = pos + Hash2( mod( pos, vec2( nCellWidth, nCellHeight ) ) );
	
				
				float dist = length( currentPoint - randomPoint );
				
				if( dist <= f1  ) 
				{
					f4 = f3;
					f3 = f2;
					f2 = f1;
					f1 = dist;
				}
				if( dist >= maxDistance ) maxDistance  = dist;
			}
		}
		
		float size = length( sizeCell );
		float mask  = ( clamp(f2 - f1, 0.2, 0.3 ) - 0.2 )* 10.;
		
		return vec3( mask, mask, mask );
}



void main(void)
{

	vec3 mask = map( 
								vec2( gl_FragCoord.x + iGlobalTime * 2., gl_FragCoord.y + iGlobalTime * 3. ), 
								nCellWidth * abs( sin( iGlobalTime * 0.2   ) ) + 10,
								nCellHeight * abs( cos( iGlobalTime * 0.01 ) ) + 2
								);
	
	gl_FragColor = vec4( mask ,1.0);
	

}