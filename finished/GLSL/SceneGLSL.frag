uniform float iGlobalTime; 
uniform vec3 iResolution; 
uniform vec4 iDate;
uniform vec3 iMouse;
uniform sampler2D iChannel0; 
uniform vec4 iLoc;

#define N_PLANE 0.
#define F_PLANE 10.
#define STEP 0.0001

int nCellWidth  = 20;
int nCellHeight = 20;

vec2 Hash2(vec2 p)
{
    float r = 523.0*sin(dot(p, vec2(53.3158, 43.6143)));
    return vec2(fract(15.32354 * r), fract(17.25865 * r));
}

vec3 map( vec2 uv )
{
		vec2 sizeCell = iResolution.xy / vec2( nCellWidth, nCellHeight ); 
		vec2 currentPos = uv / sizeCell;
		currentPos = floor( currentPos );
		
		vec2 currentPoint = uv / sizeCell;
		
		
		float f1				   = 10000.0;
		float f2                 = 10000.0;
		float f3				   = 10000.0;
		float f4				   = 10000.0;
		float maxDistance = 0.0;
		
		for( int w = -1; w <= 1; w++ )
		{
			for( int h = -1; h <= 1; h++ )
			{
				vec2	pos = currentPos + vec2( w, h );
	
				vec2 randomPoint = pos + Hash2( mod( pos, vec2( nCellWidth, nCellHeight ) ) );
	
				
				float dist = length( currentPoint - randomPoint );
				
				if( dist <= f1  ) 
				{
					f4 = f3;
					f3 = f2;
					f2 = f1;
					f1 = dist;
				}
				if( dist >= maxDistance ) maxDistance  = dist;
			}
		}
		
		float size = length( sizeCell );
		float mask  = ( clamp(f2 - f1, 0.2, 0.3 ) - 0.2 )* 10.;
		
		return vec3( mask, mask, mask );
}

float sphere( vec3 pos, vec3 or, float radius, float variance )
{
	return length( pos - or ) - radius - variance * 0.02;
}

float plane( vec3 pos, float height )
{
	return pos.y - height;
}

vec2 createWorld( vec3 pos, float variance, out int type )
{
	vec2 d1 = vec2( sphere( pos, vec3( 0. ), 1., variance ), 1.0 );
	vec2 d2 = vec2( plane( pos, -1. ), 2. );
	
	type = 0;
	
	if( d2.x < d1.x )
	{
		d1 = d2;
		type = 1;
	}
	
	return d1;
}



vec3 getNormal( vec3 pos, float variance  )
{
	int type = 0;
	
	return normalize (
					vec3(
						createWorld( vec3( pos.x + STEP, pos.y,  pos.z), variance, type  ).x - createWorld( pos, variance, type ). x,
						createWorld( vec3( pos.x , pos.y + STEP, pos.z), variance, type  ).x - createWorld( pos, variance, type ). x,
						createWorld( vec3( pos.x, pos.y, pos.z + STEP ), variance, type  ).x - createWorld( pos, variance, type ). x
					)
				);
	 
}


float getShadow( vec3 ro, vec3 rd, float variance )
{
	float res = 1.;
	int type = 0;
	
	for( float t = 0.1 ; t < 8.; )
	{
		float h = createWorld( ro + t * rd, variance, type ).x;
		
		if( h < 0.01 )
		{
			return 0.0;		
		}
		
		res = min( res, 4.0 * h/t );
		t += h;
	}
	
	return res;
}

vec2 castRay( vec3 ro, vec3 rd, float variance, out int type )
{
	for( float t = N_PLANE; t <= F_PLANE; )
	{
		vec3 pos = ro + t * rd;
		
		vec2 resultIntersection = createWorld( pos, variance, type );
		
		if( resultIntersection.x < 0.0001 )
		{
			return vec2( t, resultIntersection.y );
		}
		
		t += resultIntersection.x;
	}
	
	return vec2( 0.0 );
	
	
}

vec3 moveCamera()
{
	return vec3(0);
}

void main()
{
	vec2 uv = -1. + 2.  * gl_FragCoord.xy / iResolution.xy;
	uv.y *= iResolution.y / iResolution.x;
	
	vec3 mask = map( gl_FragCoord.xy );
	vec3 light   = normalize( vec3( 1., 0.8, 0.6 ) );
	// camera movement  
    
	vec3 ro = vec3( 0, 2., 4. );
    vec3 ta = vec3( 0.0, 0.0, 0.0 );
    // camera matrix
    vec3 ww = normalize( ta - ro );
    vec3 uu = normalize( cross(ww,vec3(0.0,1.0,0.0) ) );
    vec3 vv = normalize( cross(uu,ww));
    // create view ray
    vec3 rd = normalize( uv.x*uu + uv.y*vv + 1.*ww );

	int type = 0;
	
	vec2 t = castRay( ro, rd, mask.x, type );
	vec3 color = vec3(0.0,0.2,0.2);
	
	if( t.y > 0.0 )
	{
		vec3 pos = ro + t.x * rd;
		
		vec3 normal = getNormal( pos, mask.x );
		
		float diff = max( 0.0, dot( normal, light ) );
		
		if( type == 0 )
		{
			
			diff *= mask.x;
		}
		
		float shadow = getShadow( pos, light, mask.x );
		
		color = vec3( 0.1 ) + vec3( 1. ) * diff * shadow ;
		
	}
	gl_FragColor = vec4(color,1.0);	
}