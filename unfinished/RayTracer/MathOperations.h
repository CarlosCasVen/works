#ifndef MATH_OPERATIONS_H
#define MATH_OPERATIONS_H

#include "Scene.h"
#include "Ray.h"

#define DELTA 0.000001f


bool IsIntersectingSphere	 ( SceneSphere*     sphere, Vector ro, Vector rd, bool culling, CollisionPointInfo& collisionInfo				                         );
bool IsIntersectingTriangle  ( SceneTriangle* triangle, Vector ro, Vector rd, bool culling, CollisionPointInfo& collisionInfo				                         );
bool IsIntersectingTriangle  ( SceneTriangle* triangle, Vector ro, Vector rd, bool culling, CollisionPointInfo& collisionInfo, Matrix& transform                     );
bool IsIntersectingModel	 ( SceneModel*       model, Vector ro, Vector rd, bool culling, CollisionPointInfo& collisionInfo, SceneTriangle* triangleIgnored = NULL );


bool TestTriangle( Vector vertex[], Vector ro, Vector rd, float* uvt, bool culling = true );
bool TestSphere( float powDistanceToCenter, float powRadious, bool culling, Vector ro, Vector rd, Vector sphereCenter, float& distanceToProjection, float& distanceRayProj );



bool IsIntersectingSphere( SceneSphere* sphere, Vector ro, Vector rd, bool culling, CollisionPointInfo& collisionInfo )
{
	Vector spherePosition = sphere->position + sphere->center;

	Vector roToCenter = spherePosition - ro;

    float powDistanceToCenter = roToCenter.PowMagnitude();
	float powRadious = sphere->radius * sphere->radius;

    float distanceToProjection = 0.0f;
    float distanceRayProj      = 0.0f;

    if( !TestSphere( powDistanceToCenter, powRadious, culling, ro, rd, spherePosition, distanceToProjection, distanceRayProj ) )
    {
        collisionInfo.m_tIntersection   = -1.0;
        return collisionInfo.m_hasIntersection = false;
    }

	float distanToIntersection = sqrtf( powRadious - distanceToProjection );
	collisionInfo.m_tIntersection = distanceRayProj;

	if( powDistanceToCenter > powRadious )
	{
		collisionInfo.m_tIntersection -= distanToIntersection;
	}
	else
	{
		collisionInfo.m_tIntersection += distanToIntersection;
	}

    //calculo de la normal
	collisionInfo.m_normal = ( ro + rd * collisionInfo.m_tIntersection ) - spherePosition;
	collisionInfo.m_normal = collisionInfo.m_normal.Normalize();

    //calculo de la coord uv
    collisionInfo.m_material.m_u = collisionInfo.m_material.m_v = 0.0f;

    //se guarda el modelo intersecado
    collisionInfo.m_objectIntersected = sphere;

    //se guarda el material 
    collisionInfo.m_material.m_pixelTexture     = sphere->m_material->GetTextureColor( collisionInfo.m_material.m_u, collisionInfo.m_material.m_v ); 

    collisionInfo.m_material.m_diffuse          = sphere->m_material->diffuse;
  
    collisionInfo.m_material.m_shininess        = sphere->m_material->shininess;

    collisionInfo.m_material.m_specular         = sphere->m_material->specular;

    collisionInfo.m_material.m_transparent      = sphere->m_material->transparent;

    collisionInfo.m_material.m_refraction_index = sphere->m_material->refraction_index; 

    collisionInfo.m_material.m_reflective       = sphere->m_material->reflective; 

	return collisionInfo.m_hasIntersection = true;
}


bool IsIntersectingTriangle( SceneTriangle* triangle, Vector ro, Vector rd, bool culling, CollisionPointInfo& collisionInfo )
{
	//obtencion de la matriz de transformacion
	Matrix translate;
	translate.SetTranslation( triangle->position );

	Matrix rotX;
	rotX.SetRotation( RotAxis( triangle->rotation.x, Vector( 1.0, 0.0, 0.0 ) ) );
	Matrix rotY;
	rotY.SetRotation( RotAxis( triangle->rotation.y, Vector( 0.0, 1.0, 0.0 ) ) );
	Matrix rotZ;
	rotZ.SetRotation( RotAxis( triangle->rotation.z, Vector( 0.0, 0.0, 1.0 ) ) );
    
	Matrix scale;
	scale.SetScale( triangle->scale );

	Matrix transform = translate * rotZ * rotY * rotX * scale;

	//obtencion de los vertices transformados
	Vector p0 = transform * triangle->vertex[ 0 ];
	Vector p1 = transform * triangle->vertex[ 1 ];
	Vector p2 = transform * triangle->vertex[ 2 ];

	float  uvt      [ 3 ];
    Vector vertices [ 3 ] = { p0, p1, p2 };

    collisionInfo.m_hasIntersection = TestTriangle( vertices, ro, rd, uvt, culling );

    if( !collisionInfo.m_hasIntersection )
    {
        return false;
    }

    Matrix rot = rotZ * rotY * rotX;

    //calculo de la normal
    collisionInfo.m_normal = ( rot * triangle->normal[ 0 ] ) * ( 1.0f - uvt[ 0 ] - uvt[ 1 ] ) + rot * ( triangle->normal[ 1 ] ) * uvt[ 0 ] + ( rot * triangle->normal[ 2 ] ) * uvt[ 1 ];
	collisionInfo.m_normal = collisionInfo.m_normal.Normalize();

    //calculo de la uv
	collisionInfo.m_tIntersection = uvt[ 2 ];

    //se guarda el modelo intersecado
    collisionInfo.m_objectIntersected = triangle;

    return collisionInfo.m_hasIntersection = true;
}


bool IsIntersectingTriangle( SceneTriangle* triangle, Vector ro, Vector rd, bool culling, CollisionPointInfo& collisionInfo, Matrix& transform )
{
	//obtencion de la matriz de transformacion
	Matrix translate;
	translate.SetTranslation( triangle->position );

	Matrix rotX;
	rotX.SetRotation( RotAxis( triangle->rotation.x, Vector( 1.0, 0.0, 0.0 ) ) );
	Matrix rotY;
	rotY.SetRotation( RotAxis( triangle->rotation.y, Vector( 0.0, 1.0, 0.0 ) ) );
	Matrix rotZ;
	rotZ.SetRotation( RotAxis( triangle->rotation.z, Vector( 0.0, 0.0, 1.0 ) ) );

	Matrix scale;
	scale.SetScale( triangle->scale );

	transform = transform * translate * rotZ * rotY * rotX * scale;

	//obtencion de los vertices transformados
	Vector p0 = transform * triangle->vertex[ 0 ];
	Vector p1 = transform * triangle->vertex[ 1 ];
	Vector p2 = transform * triangle->vertex[ 2 ];

    float  uvt      [ 3 ];
    Vector vertices [ 3 ] = { p0, p1, p2 };

    collisionInfo.m_hasIntersection = TestTriangle( vertices, ro, rd, uvt, culling );

    if( !collisionInfo.m_hasIntersection )
    {
        return false;
    }

    Matrix rot = rotZ * rotY * rotX;

    float a = ( 1.0f - uvt[ 0 ] - uvt[ 1 ] );
    float b = uvt[ 0 ];
    float c = uvt[ 1 ];

    //calculo de la normal
    Vector normalP0 = triangle->normal[0];
    Vector normalP1 = triangle->normal[1];
    Vector normalP2 = triangle->normal[2];
    collisionInfo.m_normal = ( rot * triangle->normal[ 0 ] ) * a + rot * ( triangle->normal[ 1 ] ) * b + ( rot * triangle->normal[ 2 ] ) * c;
	collisionInfo.m_normal = collisionInfo.m_normal.Normalize();

    //calculo de la uv
	collisionInfo.m_material.m_u  = uvt[ 0 ];
    collisionInfo.m_material.m_v  = uvt[ 1 ];
    collisionInfo.m_tIntersection = uvt[ 2 ];
    
    //se guarda el modelo intersecado
    collisionInfo.m_objectIntersected = triangle;
    
    //calculo del material
    collisionInfo.m_material.m_pixelTexture     = triangle->m_material[ 0 ]->GetTextureColor( b, c ) * a + 
                                                  triangle->m_material[ 1 ]->GetTextureColor( b, c ) * b + 
                                                  triangle->m_material[ 2 ]->GetTextureColor( b, c ) * c; 

    collisionInfo.m_material.m_diffuse          = triangle->m_material[ 0 ]->diffuse * a + 
                                                  triangle->m_material[ 1 ]->diffuse * b + 
                                                  triangle->m_material[ 2 ]->diffuse * c;
  
    collisionInfo.m_material.m_shininess        = triangle->m_material[ 0 ]->shininess * a + 
                                                  triangle->m_material[ 1 ]->shininess * b + 
                                                  triangle->m_material[ 2 ]->shininess * c;

    collisionInfo.m_material.m_specular         = triangle->m_material[ 0 ]->specular * a + 
                                                  triangle->m_material[ 1 ]->specular * b + 
                                                  triangle->m_material[ 2 ]->specular * c;

    collisionInfo.m_material.m_transparent      = triangle->m_material[ 0 ]->transparent * a + 
                                                  triangle->m_material[ 1 ]->transparent * b + 
                                                  triangle->m_material[ 2 ]->transparent * c;

    collisionInfo.m_material.m_refraction_index = triangle->m_material[ 0 ]->refraction_index * a + 
                                                  triangle->m_material[ 1 ]->refraction_index * b + 
                                                  triangle->m_material[ 2 ]->refraction_index * c; 

    collisionInfo.m_material.m_reflective       = triangle->m_material[ 0 ]->reflective * a + 
                                                  triangle->m_material[ 1 ]->reflective * b + 
                                                  triangle->m_material[ 2 ]->reflective * c; 

    return collisionInfo.m_hasIntersection;
}


bool IsIntersectingModel( SceneModel* model, Vector ro, Vector rd, bool culling, CollisionPointInfo& collisionInfo, SceneTriangle* triangleIgnored )
{
	CollisionPointInfo partialInfo;
    collisionInfo.m_tIntersection = -1.0f;
	
    Matrix translate;
	translate.SetTranslation( model->position );

	Matrix rotX;
	rotX.SetRotation( RotAxis( model->rotation.x, Vector( 1.0, 0.0, 0.0 ) ) );
	Matrix rotY;
	rotY.SetRotation( RotAxis( model->rotation.y, Vector( 0.0, 1.0, 0.0 ) ) );
	Matrix rotZ;
	rotZ.SetRotation( RotAxis( model->rotation.z, Vector( 0.0, 0.0, 1.0 ) ) );

	Matrix scale;
	scale.SetScale( model->scale );

	Matrix transform = translate * rotZ * rotY * rotX * scale;

	for( unsigned int index = 0; index < model->GetNumTriangles(); index++ )
	{
        SceneTriangle* triangle = model->GetTriangle( index );

        if( triangle == triangleIgnored )
        {
            continue;
        }
        
        if( IsIntersectingTriangle( triangle, ro, rd, culling, partialInfo, transform ) )
		{
			if( collisionInfo.m_tIntersection == -1.0f || collisionInfo.m_tIntersection > partialInfo.m_tIntersection )
			{
				collisionInfo = partialInfo;
			}
		}
	}

	return collisionInfo.m_hasIntersection;
}

///TESTIG DE RAYO CON PRIMITIVAS
bool TestTriangle( Vector vertex[], Vector ro, Vector rd, float* uvt, bool culling )
{
    Vector e1 = vertex[ 1 ] - vertex[ 0 ];
	Vector e2 = vertex[ 2 ] - vertex[ 0 ];

	Vector q = rd.Cross( e2 );

	float a = e1.Dot( q );

    if( a > -1.0 && !culling )
        int t = 0;
	if( a < DELTA && ( !culling && a > -DELTA ) )
	{
        return false;
	}
 
	Vector s = ro - vertex[ 0 ];

	float f = 1.0f / a;
	uvt[ 0 ] = f * s.Dot( q );

	if( uvt[ 0 ] < 0.0f )
	{
        return false;
	}

	Vector r = s.Cross( e1 );

	uvt[ 1 ] = f * rd.Dot( r );

	if( uvt[ 1 ] < 0.0f || uvt[ 0 ] + uvt[ 1 ] > 1.0f )
	{
		return false;
	}

    uvt[ 2 ] = f * r.Dot( e2 );
    
    return true;
}


bool TestSphere( float powDistanceToCenter, float powRadious, bool culling, Vector ro, Vector rd, Vector sphereCenter, float& distanceToProjection, float& distanceRayProj )
{
    Vector roToCenter = sphereCenter - ro;

    if( powDistanceToCenter < powRadious && culling )
    {
        return false;
    }

	distanceRayProj = rd.Dot( roToCenter );

	float powDistanceRayProj = distanceRayProj * distanceRayProj;
	distanceToProjection = powDistanceToCenter - powDistanceRayProj;

	if( distanceToProjection > powRadious )
	{
		return false;
	}

    return true;
}


#endif MATH_OPERATIONS_H

