#ifdef _OS_X_
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>	

#elif defined(WIN32)
#include <windows.h>
#include "GL/gl.h"
#include "GL/glu.h"
#include "GL/glut.h"

#else
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif

#include "Scene.h"
#include "RayTrace.h"



#define DELTA 0.000001f
#define REBOUNDS 4

// -- Main Functions --
// - CalculatePixel - Returns the Computed Pixel for that screen coordinate
Vector RayTrace::CalculatePixel (int screenX, int screenY)
{
   /*
   -- How to Implement a Ray Tracer --

   This computed pixel will take into account the camera and the scene
   and return a Vector of <Red, Green, Blue>, each component ranging from 0.0 to 1.0

   In order to start working on computing the color of this pixel,
   you will need to cast a ray from your current camera position
   to the image-plane at the given screenX and screenY
   coordinates and figure out how/where this ray intersects with 
   the objects in the scene descriptor.
   The Scene Class exposes all of the scene's variables for you 
   through its functions such as m_Scene.GetBackground (), m_Scene.GetNumLights (), 
   etc. so you will need to use those to learn about the World.

   To determine if your ray intersects with an object in the scene, 
   you must calculate where your objects are in 3D-space [using 
   the object's scale, rotation, and position is extra credit]
   and mathematically solving for an intersection point corresponding to that object.

   For example, for each of the spheres in the scene, you can use 
   the equation of a sphere/ellipsoid to determine whether or not 
   your ray from the camera's position to the screen-pixel intersects 
   with the object, then from the incident angle to the normal at 
   the contact, you can determine the reflected ray, and recursively 
   repeat this process capped by a number of iterations (e.g. 10).

   Using the lighting equation & texture to calculate the color at every 
   intersection point and adding its fractional amount (determined by the material)
   will get you a final color that returns to the eye at this point.
   */

   Scene &la_escena = m_Scene;
   
   if (screenX == 162  && screenY == 141 )
   {
      int kk=0;
   }
   
   if ((screenX < 0 || screenX > Scene::WINDOW_WIDTH - 1) ||
      (screenY < 0 || screenY > Scene::WINDOW_HEIGHT - 1))
   {
      // Off the screen, return black
      return Vector (0.0f, 0.0f, 0.0f);
   }

  // printf( "pixel: %d, %d Interseca: \t", screenX, screenY );

   if( la_escena.montecarlo )
   {
       return Montecarlo( screenX, screenY );
   }
   else if( la_escena.supersample )
   {
       return Supersampling( screenX, screenY );
   } 
   else
   {
       return RayTracer( screenX, screenY );
   }

   
}

Vector RayTrace::Montecarlo( int screenX, int screenY )
{
    return Vector( 0.0, 0.0, 0.0 );
}


Vector RayTrace::RayTracer( int screenX, int screenY )
{
   return Trace( screenX, screenY, Scene::WINDOW_WIDTH, Scene::WINDOW_HEIGHT );
  
}
   

Vector RayTrace::Supersampling( int screenX, int screenY )
{
    screenX *= 4;
    screenY *= 4;

    Vector color = Vector( 0,0,0);
    int counter = 0;

    for( int x = 0; x < 4; x++ )
    {
        for( int y = 0; y < 4; y++)
        {
            color = color + Trace( screenX + ( x - 2 ), screenY + ( y - 2 ), 4 * Scene::WINDOW_WIDTH, 4 * Scene::WINDOW_HEIGHT );
            counter++;
        }

    }
    return color * ( 1.0f / counter );
}


Vector RayTrace::Trace( int screenX, int screenY, int width, int height )
{
   Scene &la_escena = m_Scene;
   Camera &la_camara = la_escena.GetCamera();
   Vector posicion = la_camara.GetPosition();

   Ray rays[ REBOUNDS + 1 ];
   int rebounds = 0;

   rays[ rebounds ] = PrepareCamera( screenX, screenY, width, height );
   
   if( !rays[ rebounds++ ].CastRay( &la_escena, la_camara.nearClip, la_camara.farClip ) )
   {
       return la_escena.GetBackground().color;
   }

   while( rebounds < REBOUNDS + 1 && rays[ rebounds - 1 ].HasCollision() )
   {
       rays[ rebounds ] = rays[ rebounds - 1 ].RefelectRay();
       rays[ rebounds ].CastRay(  &la_escena, la_camara.nearClip, la_camara.farClip );
       rebounds++;
   }

   //Si se produce la interseccion se calcula la iluminacion de phong 

   Vector intensity = Vector( 0.0, 0.0, 0.0 );

   for( unsigned int index = 0; index < la_escena.GetNumLights(); index++ )
   {
       SceneLight* light = la_escena.GetLight( index );
       Vector illumination = Vector( 1.0f, 1.0f, 1.0f );

       for( unsigned int nRay = 0; nRay < rebounds - 1; nRay++ )
       {
            Vector                point            = rays[ nRay ].GetCollisionPoint();
            Vector                lightDir         = light->position - point;
            float                 distanceToLight  = lightDir.Magnitude();
            Ray                   shadowRay        = Ray( point, lightDir );
            
            CollisionMaterialInfo material         = rays[ nRay ].GetMaterial();

            float att = 1.0f / ( 1.0f + light->attenuationConstant + light->attenuationLinear * distanceToLight + pow( light->attenuationQuadratic, 2.0f ) * pow( distanceToLight, 2.0f ) ); 

            if( shadowRay.CastRay( &la_escena, 0.0f, distanceToLight, rays[ nRay ].GetCollisionObject(), false ) )
            {
                continue;
            }
            
            Vector reflection  = ( shadowRay.GetDirection() * -1.0f ).Reflect( rays[ nRay ].GetNormal() );
            Vector colorAtt    = light->color * att;  

            illumination = illumination                                                                                                             *  
                           ( colorAtt * ( material.m_diffuse  + material.m_pixelTexture) * rays[ nRay ].GetNormal().Dot( shadowRay.GetDirection() ) +
                             light->color * att * pow( reflection.Dot( rays[ nRay ].GetDirection() * -1.0f ), material.m_shininess ) );
       }
       intensity = intensity + illumination;
   }
   
   Vector ambientLight = la_escena.GetBackground().ambientLight;

   return ambientLight + intensity;
}


Ray RayTrace::PrepareCamera( int screenX, int screenY, int width, int height )
{
    Scene &la_escena = m_Scene;
    Camera &la_camara = la_escena.GetCamera();
    
    la_camara.Prepare( width, height );
   
    return la_camara.GetRay( screenX, screenY );
}