#include "Utils.h"
#include "Ray.h"
#include "Scene.h"
#include "MathOperations.h"


CollisionPointInfo::CollisionPointInfo()
{
    m_objectIntersected = NULL;
    m_tIntersection     = -1.0f;
    m_color             = Vector( 0.0f, 0.0f, 0.0f );
    m_normal            = Vector( 0.0f, 0.0f, 0.0f );
    m_hasIntersection   = false;
    m_u = m_v           = 0.0f;
}

Ray::Ray()
{
}

Ray::Ray( Vector origin, Vector direction )
{
    m_origin = origin;
	m_direction = direction.Normalize();
}


Ray::~Ray()
{

}

bool Ray::CastRay( const Scene* scene, float nearPlane, float farPlane, SceneObject* objectIgnored, bool culling )
{
	Scene* s = (Scene*)scene;
    CollisionPointInfo partialInfo;
	
	for( unsigned int index = 0; index < s->GetNumObjects(); index++ )
	{
		SceneObject* object = s->GetObject( index );
		char debug[128];

		if( object == objectIgnored )
		{
			continue;
		}
		
		switch( object->type )
		{
			case SceneObjectType::Triangle:
				IsIntersectingTriangle( ( SceneTriangle* ) object, m_origin, m_direction, culling, partialInfo );
				break;
			case SceneObjectType::Sphere:
				IsIntersectingSphere( ( SceneSphere* ) object, m_origin, m_direction, culling, partialInfo );
				break;
            case SceneObjectType::Model:
				IsIntersectingModel( ( SceneModel* ) object, m_origin, m_direction, culling, partialInfo, (SceneTriangle*) object );
				break;
			default:
				break;
		}

		if( partialInfo.m_hasIntersection && partialInfo.m_tIntersection >= nearPlane && partialInfo.m_tIntersection <= farPlane )
		{
			if( m_collisionInfo.m_tIntersection == -1.0f || m_collisionInfo.m_tIntersection > partialInfo.m_tIntersection )
			{
				m_collisionInfo                     = partialInfo;
                m_collisionInfo.m_objectIntersected = object;
                m_collisionInfo.m_hasIntersection   = true;
			}
		}
	}

	return m_collisionInfo.m_hasIntersection;
}

void Ray::SetOrigin( Vector origin )
{
	m_origin = origin;
}


void Ray::SetDirection( Vector direction )
{
	m_direction = direction;
}


Vector Ray::GetCollisionPoint()
{
	return m_origin + m_direction * m_collisionInfo.m_tIntersection;
}


Vector Ray::GetColorCollision()
{
	return m_collisionInfo.m_color;
}


Vector Ray::GetOrigin()
{
	return m_origin;
}


Vector Ray::GetDirection()
{
	return m_direction;
}

Vector Ray::GetNormal()
{
	return m_collisionInfo.m_normal;
}

float Ray::GetDistanceIntersection()
{
	return m_collisionInfo.m_tIntersection;
}

SceneObject* Ray::GetCollisionObject()
{
	return m_collisionInfo.m_objectIntersected;
}

CollisionMaterialInfo Ray::GetMaterial()
{
    return m_collisionInfo.m_material;
}

Ray Ray::RefelectRay()
{
    if( !m_collisionInfo.m_hasIntersection )
    {
        return Ray( Vector( 0.0f, 0.0f, 0.0f ), Vector( 0.0f, 0.0f, -1.0f ) );
    }
    
    Vector dirReflected = m_direction.Reflect( m_collisionInfo.m_normal );

    return Ray( m_origin + m_direction * m_collisionInfo.m_tIntersection, dirReflected.Normalize() );
}

bool Ray::HasCollision()
{
    return m_collisionInfo.m_hasIntersection;
}