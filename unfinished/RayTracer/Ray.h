#ifndef RAY_H
#define RAY_H

#include "Utils.h"

#define DEFAULT_FAR_PLANE 10000.0f

class SceneMaterial;
class Scene;
class SceneObject;

struct CollisionMaterialInfo
{
    float  m_u;
    float  m_v;
    Vector m_pixelTexture;
    Vector m_diffuse;
    Vector m_specular;
	float  m_shininess;
	Vector m_transparent;
	Vector m_reflective;
	Vector m_refraction_index;

    CollisionMaterialInfo()
    {
        m_u = m_v = 0.0;
        m_pixelTexture = m_diffuse = m_specular = m_transparent = m_reflective = m_refraction_index = Vector( 0.0f, 0.0f, 0.0f );
        m_shininess = 1.0f;
    }
};

struct CollisionPointInfo
{
    SceneObject*          m_objectIntersected;
	float		          m_tIntersection;
	Vector		          m_color;
	Vector		          m_normal;
	bool		          m_hasIntersection;
    float                 m_u;
    float                 m_v;
    CollisionMaterialInfo m_material;

    CollisionPointInfo();
};

class Ray
{
public:
    Ray ();
	Ray ( Vector origin, Vector direction );
	~Ray(								  );

	bool CastRay( const Scene* scene, float nearPlane = 0.0f, float farPlane = DEFAULT_FAR_PLANE, SceneObject* objectIgnored = NULL, bool culling = true );
	
	void SetOrigin	 ( Vector origin	);
	void SetDirection( Vector direction );

    bool		          HasCollision  		 ();
	Vector		          GetCollisionPoint		 ();
	Vector		          GetColorCollision		 ();
	SceneObject*          GetCollisionObject	 ();
	Vector		          GetOrigin				 ();
	Vector		          GetDirection			 ();
	Vector		          GetNormal  			 ();
	float		          GetDistanceIntersection();
    CollisionMaterialInfo GetMaterial            (); 

    Ray RefelectRay();

private:
	Vector m_origin;
	Vector m_direction;
	
	CollisionPointInfo m_collisionInfo;
};


#endif