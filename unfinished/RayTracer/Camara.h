#ifndef CAMARA_H
#define CAMARA_H


#include "Utils.h"
#include "Ray.h"

/*
Camera Class - Class to help keep track of camera movements

This class keeps the camera vectors together and adds a bit of functionality around it
NOTE: This is unoptimized code for better readability
*/
class Camera
{
public:
	Vector position;
	Vector target;
	Vector up;
	float fieldOfView;
	float nearClip, farClip;

	bool   m_isPrepare;
	float  m_width;
	float  m_height;
	Vector m_rightCam;
	Vector m_upCam;
	Vector m_frontCam;

	// -- Constructors & Destructors --
	// - Default Constructor - Initializes to Vector <0, 0, 0>
	Camera( void ) {}

	// - Parameter Constructor - Initializes to Vector <a, b, c>
	Camera( Vector p, Vector t, Vector u ) { position = p; target = t; up = u; m_isPrepare = false; }

	// - Default Destructor -
	~Camera() {}

	// -- Accessor Functions --
	// - GetPosition - Returns the position vector of the camera
	Vector GetPosition( void ) { return position; }

	// - SetPosition - Sets the position vector of the camera
	void SetPosition( const Vector vec ) { position = vec; }

	// - GetTarget - Returns the target vector of the camera
	Vector GetTarget( void ) { return target; }

	// - SetTarget - Sets the target vector of the camera
	void SetTarget( const Vector vec ) { target = vec; }

	// - GetUp - Returns the up vector of the camera
	Vector GetUp( void ) { return up; }

	// - SetUp - Sets the up vector of the camera
	void SetUp( const Vector vec ) { up = vec; }

	// - GetFOV - Returns the Field of View
	float GetFOV( void ) { return fieldOfView; }

	// - SetFOV - Sets of Field of View
	void SetFOV( float fov ) { fieldOfView = fov; }

	// - GetNearClip - Returns the Near-Clip Distance
	float GetNearClip( void ) { return nearClip; }

	// - SetNearClip - Sets the Near-Clip Distance
	void SetNearClip( float nc ) { nearClip = nc; }

	// - GetFarClip - Returns the Far-Clip Distance
	float GetFarClip( void ) { return farClip; }

	// - SetNearClip - Sets the Near-Clip Distance
	void SetFarClip( float fc ) { farClip = fc; }

	// - Configure de camera
	void Prepare( int width, int height )
	{
		if( m_isPrepare )
		{
			return;
		}
		m_isPrepare = true;

		m_frontCam = target - position;
		m_frontCam = m_frontCam.Normalize();

		m_rightCam = m_frontCam.Cross( up );
		m_rightCam = m_rightCam.Normalize();

		m_upCam = m_rightCam.Cross( m_frontCam );
		m_upCam.Normalize();

		m_width = ( float ) width / 2.0f;
		m_height = ( float ) height / 2.0f;

		m_frontCam = m_frontCam * ( m_height /( float )( DegTan( (double)( fieldOfView / 2.0f ) ) ) );
	}

	Ray GetRay( float u, float v )
	{
		Vector dest = position + m_frontCam + m_rightCam * ( u - m_width ) + m_upCam * ( v - m_height );

		return Ray( position, dest - position );
	}
};


#endif