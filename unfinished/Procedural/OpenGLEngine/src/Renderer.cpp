#include <stdio.h>
#include "glew.h"
#include "freeglut.h"
#include "../includes/Locations.h"
#include "../includes/Renderer.h"
#include "../includes/IScreen.h"
#include "../includes/Engine.h"
#include "../includes/Array.h"
#include "../includes/DataBase.h"
#include "../includes/IScreen.h"
#include <stddef.h>
#include "../includes/Vertex.h"


#define ERROR_BUFFER_SIZE 1024
#define BACKGROUND_COLOR 255,255,255,0

#define LIGHT_VERTEX_SHADER_PATH "data/vLightShader.txt"
#define LIGHT_FRAGMENT_SHADER_PATH "data/fLightShader.txt"

#define POST_PROCESSING_VERTEX_SHADER_PATH "data/vPostShader.txt"
#define POST_PROCESSING_FRAGMENT_SHADER_PATH "data/fPostShader.txt"

#define POST_PROCESSING_VERTEX_SHADER_PATH "data/vPostShader.txt"
#define POST_PROCESSING_FRAGMENT_SHADER_PATH "data/fPostShader.txt"

#define CUBE_MAP_VERTEX_SHADER_PATH "data/vCubeMapShader.txt"
#define CUBE_MAP_FRAGMENT_SHADER_PATH "data/fCubeMapShader.txt"

const GLenum m_geometryFboColorAttachments[] = {
                                                    GL_COLOR_ATTACHMENT0,
                                                    GL_COLOR_ATTACHMENT1,
                                                    GL_COLOR_ATTACHMENT2,
                                                    GL_DEPTH_ATTACHMENT
                                                };

const GLenum m_lightFboColorAttachment[] = {
                                                GL_COLOR_ATTACHMENT0,                                               
                                            };

Renderer* m_renderer = NULL;
//----------------------------------------------
//
//----------------------------------------------
Renderer& Renderer::Instance()
{
    if( !m_renderer )
    {
        m_renderer = new Renderer();
    }

    return *m_renderer;
}
//----------------------------------------------
//
//----------------------------------------------
bool Renderer::Init()
{

    //Init el contexto (inicializacion glew glut )
    int argc = 0;

    glutInit( &argc, NULL );

    glutInitContextVersion( 4, 3 );
    glutInitContextFlags( GLUT_FORWARD_COMPATIBLE );
    glutInitContextProfile( GLUT_CORE_PROFILE );
    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );

    glutInitWindowSize( 1024, 800 );
    glutInitWindowPosition( 0, 0 );

    glutCreateWindow( "glsl3" );

    glutReshapeFunc( Engine::ReshapeFunc );
    glutDisplayFunc( Engine::Display );
    glutIdleFunc( Engine::MainLoop );
    glutKeyboardFunc( Engine::Input );

    glewExperimental = GL_TRUE;
    GLenum err = glewInit();

    if( GLEW_OK != err )
    {
        return false;
    }

    const GLubyte *oglVersion = glGetString( GL_VERSION );
    printf( "This system supports OpenGL Version %s.\n", oglVersion );
    
    //CREACION DEL QUAD DE PANTALLA
    CreateQuad();

    //CREACION DEL PROGRAMA DE ILUMINACION DEL QUAD
    m_lightShader = new Shader( LIGHT_VERTEX_SHADER_PATH, LIGHT_FRAGMENT_SHADER_PATH );

    //CRACION DEL SHADER DE POSTPROCESO
    m_postProccessingShader = new Shader( POST_PROCESSING_VERTEX_SHADER_PATH, POST_PROCESSING_FRAGMENT_SHADER_PATH );

    //CREACION DE LOS FBOS
    uint16 width = 0, height = 0;
    
    IScreen::Instance().GetWindowSize( &width, &height );

    InitGeometryFBO( width, height );
    InitLightFBO   ( width, height );

    return true;
}
//----------------------------------------------
//
//----------------------------------------------
void Renderer::End()
{
    glDeleteTextures( NUMBER_TEXTURES_GEOMETRY_FBO - 1, m_texturesGeometryFbo );
    glDeleteTextures( NUMBER_TEXTURES_LIGHT_FBO       , m_texturesLightFbo    );

    glDeleteRenderbuffers( 1, &m_texturesGeometryFbo[ NUMBER_TEXTURES_GEOMETRY_FBO - 1 ] );
   
    glDeleteFramebuffers( 1, &m_gFbo );
    glDeleteFramebuffers( 1, &m_lFbo );

    delete m_lightFboColorAttachment;
    delete m_postProccessingShader;

    if( m_renderer )
    {
        delete m_renderer;
    }

    m_renderer = NULL;
}
//----------------------------------------------
//
//----------------------------------------------
void Renderer::ConfigRender()
{
    glEnable( GL_DEPTH_TEST );
    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    //glEnable( GL_CULL_FACE );
}
//----------------------------------------------
//
//----------------------------------------------
uint32 Renderer::CreateVBO()
{
    uint32 id = 0;
    glGenBuffers( 1, &id );

    return id;
}
//----------------------------------------------
//
//----------------------------------------------
uint32 Renderer::CreateVAO()
{
    uint32 id = 0;
    glGenVertexArrays( 1, &id );

    return id;
}
//----------------------------------------------
//
//----------------------------------------------
void Renderer::BindVBO( uint32 id, int type )
{
    glBindBuffer( type, id );
}
//----------------------------------------------
//
//----------------------------------------------
void Renderer::BindVAO( uint32 id )
{
    glBindVertexArray( id );
}
//----------------------------------------------
//
//----------------------------------------------
void Renderer::FreeVBOBuffers( uint32 id )
{
    glDeleteBuffers( 1, &id );
}
//----------------------------------------------
//
//----------------------------------------------
void Renderer::FreeVAOBuffers( uint32 id )
{
    glDeleteVertexArrays( 1, &id );
}
//----------------------------------------------
//
//----------------------------------------------
uint32 Renderer::CreateTexture()
{
    uint32 id = 0;

    glGenTextures( 1, &id );

    return id;
}
//----------------------------------------------
//
//----------------------------------------------
void Renderer::BindTexture2D( uint32 texture )
{
    glBindTexture( GL_TEXTURE_2D, texture );
}
//----------------------------------------------
//
//----------------------------------------------
void Renderer::ConfigTexture2D()
{
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
    glGenerateMipmap( GL_TEXTURE_2D );
}
//----------------------------------------------
//
//----------------------------------------------
void Renderer::ActivateTexture( uint32 textureUnit, uint32 texture, int location )
{
    glActiveTexture( GL_TEXTURE0 + textureUnit );
    glBindTexture  ( GL_TEXTURE_2D, texture    );
}
//----------------------------------------------
//
//----------------------------------------------
void Renderer::FreeTexture( uint32 texture )
{
    glDeleteTextures( 1, &texture );
}
//----------------------------------------------
//
//----------------------------------------------
void Renderer::ConfigureCubeMap()
{
    glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE );
}
//----------------------------------------------
//
//----------------------------------------------
void Renderer::DataFaceCubeMap( uint32 face, uint8* data, uint32 width, uint32 height )
{
    glTexImage2D( GL_TEXTURE_CUBE_MAP_POSITIVE_X + face, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data );
}
//----------------------------------------------
//
//----------------------------------------------
void Renderer::SetUniformMatrix( uint32 location, Matrix4 matrix )
{
    glUniformMatrix4fv( location, 1, GL_FALSE, &matrix[ 0 ] );
}
//----------------------------------------------
//
//----------------------------------------------
void Renderer::SetUniformInt( uint32 location, int value )
{
    glUniform1i( location, value );
}
//----------------------------------------------
//
//----------------------------------------------
void Renderer::SetUniformFloat( uint32 location, float value )
{
    glUniform1f( location, value );
}
//----------------------------------------------
//
//----------------------------------------------
void Renderer::SetUniformVec3( uint32 location, Vector3 position )
{
    glUniform3f(location, position.X(), position.Y(), position.Z() );
}
//----------------------------------------------
//
//----------------------------------------------
void Renderer::SetUniformVec4( uint32 location, float* position )
{
    glUniform4f( location, position[ 0 ], position[ 1 ], position[ 2 ], position[ 3 ] );
}
//----------------------------------------------
//
//----------------------------------------------
void Renderer::DeferredShading()
{
    glFrontFace( GL_CCW );
  //  glCullFace( GL_BACK );
  //  glEnable( GL_CULL_FACE );

    uint16 width,height;

    IScreen::Instance().GetWindowSize( &width, &height );

    glViewport( 0, 0, width, height );

    ResizeTexturesFBOs( width, height );

    GeometryRender();

    LightRender   ();
    PostProcessing();

    //SWAP BUFFERS
    SwapBuffers();
}
//----------------------------------------------
//
//----------------------------------------------
void Renderer::SwapBuffers()
{
    glutSwapBuffers();
}
//----------------------------------------------
//
//----------------------------------------------
Renderer::Renderer()
{
    m_gFbo = m_lFbo = 0;
    m_vaoQuad = m_vboQuad = m_eboQuad = 0;
    
    m_lightShader = m_postProccessingShader = NULL;
  
}
//----------------------------------------------
//
//----------------------------------------------
Renderer::~Renderer()
{
}
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
//----------------------------------------------
//
//----------------------------------------------
bool Renderer::InitGeometryFBO( uint16 width, uint16 height )
{
    //INICIALIZACION DEL GEOMETRY FBO
    glGenFramebuffers( 1             , &m_gFbo );
    glBindFramebuffer( GL_FRAMEBUFFER, m_gFbo  );

    //CREACION DE LOS BUFFERES
    glGenTextures     ( NUMBER_TEXTURES_GEOMETRY_FBO - 1, m_texturesGeometryFbo       );
    glGenRenderbuffers( 1, &m_texturesGeometryFbo[ NUMBER_TEXTURES_GEOMETRY_FBO - 1 ] );

    //CONFIG DEL RENDER BUFFER
    glBindRenderbuffer       ( GL_RENDERBUFFER, m_texturesGeometryFbo[ NUMBER_TEXTURES_GEOMETRY_FBO - 1 ] );
    glRenderbufferStorage    ( GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, width, height );
    glFramebufferRenderbuffer( 
                                GL_FRAMEBUFFER, m_geometryFboColorAttachments[ NUMBER_TEXTURES_GEOMETRY_FBO - 1 ], 
                                GL_RENDERBUFFER, m_texturesGeometryFbo[ NUMBER_TEXTURES_GEOMETRY_FBO - 1 ] 
                              );
    glBindRenderbuffer       ( GL_RENDERBUFFER, 0 );

    //CONFIG TEXTURES
    for( unsigned int index = 0; index < NUMBER_TEXTURES_GEOMETRY_FBO - 1; index++ )
    {
        glBindTexture( GL_TEXTURE_2D, m_texturesGeometryFbo[ index ] );
        glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, NULL );

        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );

        glFramebufferTexture2D( 
                                GL_FRAMEBUFFER, m_geometryFboColorAttachments[ index ], 
                                GL_TEXTURE_2D , m_texturesGeometryFbo        [ index ], 0 
                              );
        
    }

    glBindTexture( GL_TEXTURE_2D, 0 );

    if( glCheckFramebufferStatus( GL_FRAMEBUFFER ) != GL_FRAMEBUFFER_COMPLETE )
    {
        glDeleteFramebuffers( 1, &m_gFbo );
        return false;
    }

    glBindFramebuffer( GL_FRAMEBUFFER, 0 );

    return true;
}
//----------------------------------------------
//
//----------------------------------------------
bool Renderer::InitLightFBO( uint16 width, uint16 height )
{
    //INICIALIZACION DEL GEOMETRY FBO
    glGenFramebuffers( 1, &m_lFbo );
    glBindFramebuffer( GL_FRAMEBUFFER, m_lFbo );

    //CREACION DE LOS BUFFERES
    glGenTextures     ( NUMBER_TEXTURES_LIGHT_FBO, m_texturesLightFbo       );

    //CONFIG TEXTURES
    glBindTexture( GL_TEXTURE_2D, m_texturesLightFbo[ 0 ] );
    glTexImage2D ( GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, NULL );

    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );

    glFramebufferTexture2D(
         GL_FRAMEBUFFER, m_lightFboColorAttachment[ 0 ],
         GL_TEXTURE_2D , m_texturesLightFbo       [ 0 ], 0
         );

    glBindTexture( GL_TEXTURE_2D, 0 );

    if( glCheckFramebufferStatus( GL_FRAMEBUFFER ) != GL_FRAMEBUFFER_COMPLETE )
    {
        glDeleteFramebuffers( 1, &m_lFbo );
        return false;
    }

    glBindFramebuffer( GL_FRAMEBUFFER, 0 );

    return true;
}
//----------------------------------------------
//
//----------------------------------------------
void Renderer::ResizeTexturesFBOs( uint16 width, uint16 height )
{
    
    //TEXTURAS DEL FBO DE LUCES
    glBindRenderbuffer( GL_RENDERBUFFER, m_texturesGeometryFbo[ NUMBER_TEXTURES_GEOMETRY_FBO - 1 ] );
    glRenderbufferStorage( GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, width, height );

    //CONFIG TEXTURES
    for( unsigned int index = 0; index < NUMBER_TEXTURES_GEOMETRY_FBO - 1; index++ )
    {
        glBindTexture( GL_TEXTURE_2D, m_texturesGeometryFbo[ index ] );
        glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, NULL );

    }

    //TEXTURAS DEL FBO DE LUCES
    //CONFIG TEXTURES
    glBindTexture( GL_TEXTURE_2D, m_texturesLightFbo[ 0 ] );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB32F, width, height, 0, GL_RGBA, GL_FLOAT, NULL );

    glBindTexture( GL_TEXTURE_2D, 0 );
}
//----------------------------------------------
//
//----------------------------------------------
void Renderer::GeometryRender()
{
    //PINTADO DEL BUFFER DE GEOMETRIA
    glBindFramebuffer( GL_FRAMEBUFFER, m_gFbo );

    glClearColor( BACKGROUND_COLOR );
    glClear( GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT );
    glEnable( GL_DEPTH_TEST );

    DataBase::Instance()->Render();

    glDrawBuffers( NUMBER_TEXTURES_GEOMETRY_FBO - 1, ( GLenum* ) &m_geometryFboColorAttachments[ 0 ] );

    glBindFramebuffer( GL_FRAMEBUFFER, 0 );

    BindVAO( 0 );
}
//----------------------------------------------
//
//----------------------------------------------
void Renderer::LightRender()
{
    //PINTADO DEL BUFFER DE ILUMINACION
    glBindFramebuffer( GL_FRAMEBUFFER, m_lFbo );

    glClearColor( BACKGROUND_COLOR );
    glClear( GL_COLOR_BUFFER_BIT );
    glDisable( GL_DEPTH_TEST );

    m_lightShader->ActiveProgram();

    DataBase::Instance()->PrepareLights();

    BindVAO( m_vaoQuad );
    BindVBO( m_eboQuad, GL_ELEMENT_ARRAY_BUFFER );

    ActivateTexture( 0, m_texturesGeometryFbo[ 0 ], 0 ); //position
    ActivateTexture( 1, m_texturesGeometryFbo[ 1 ], 1 ); //diffuse
    ActivateTexture( 2, m_texturesGeometryFbo[ 2 ], 2 ); //normal

    glDrawElements( GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0 );
    
    BindVBO( 0, GL_ELEMENT_ARRAY_BUFFER );
    BindVAO( 0 );

    glDrawBuffers( NUMBER_TEXTURES_LIGHT_FBO, ( GLenum* ) &m_lightFboColorAttachment[ 0 ] );

    m_lightShader->DescActiveProgram();
}
//----------------------------------------------
//
//----------------------------------------------
void Renderer::PostProcessing()
{
    //PINTADO EN PANTALLA Y POSTPROCESO
    glBindFramebuffer( GL_FRAMEBUFFER, 0 );

    glClearColor( BACKGROUND_COLOR );
    glClear( GL_COLOR_BUFFER_BIT );
    glDisable( GL_DEPTH_TEST );

    m_postProccessingShader->ActiveProgram();

    ActivateTexture( 0, m_texturesLightFbo[ 1 ], 0 );

    BindVAO( m_vaoQuad );
    BindVBO( m_eboQuad, GL_ELEMENT_ARRAY_BUFFER );

    glBindTexture( GL_TEXTURE_2D, m_texturesLightFbo[ 0 ] );
    
    glDrawElements( GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0 );

    BindVBO( 0, GL_ELEMENT_ARRAY_BUFFER );
    BindVAO( 0 );

    m_postProccessingShader->DescActiveProgram();
}
//----------------------------------------------
//
//----------------------------------------------
void Renderer::CreateQuad()
{
    m_vaoQuad = CreateVAO();
    m_vboQuad = CreateVBO();
    m_eboQuad = CreateVBO();

    Vertex quad[] = {
        Vertex( Vector3( 1.0f, 1.0f, 0.0f ), 1.0f, 1.0f, Vector3() ),
        Vertex( Vector3( 1.0f, -1.0f, 0.0f ), 1.0f, 0.0f, Vector3() ),
        Vertex( Vector3( -1.0f, -1.0f, 0.0f ), 0.0f, 0.0f, Vector3() ),
        Vertex( Vector3( -1.0f, 1.0f, 0.0f ), 0.0f, 1.0f, Vector3() )
    };

    unsigned int indexes[] = {
        1, 0, 3,
        2, 1, 3
    };

    BindVAO( m_vaoQuad );
    BindVBO( m_vboQuad, GL_ARRAY_BUFFER );
    BindVBO( m_eboQuad, GL_ELEMENT_ARRAY_BUFFER );

    glBufferData( GL_ARRAY_BUFFER, sizeof( quad ), quad, GL_STATIC_DRAW );

    glEnableVertexAttribArray( 0 );
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, sizeof( Vertex ), ( const void* ) offsetof( Vertex, m_position ) );

    glEnableVertexAttribArray( 1 );
    glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, sizeof( Vertex ), ( const void* ) offsetof( Vertex, m_u ) );

    glBufferData( GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof( unsigned int ), indexes, GL_STATIC_DRAW );

    BindVBO( 0, GL_ELEMENT_ARRAY_BUFFER );
    BindVBO( 0, GL_ARRAY_BUFFER );
    BindVAO( 0 );
}
