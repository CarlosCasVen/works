#include "../includes/Shader.h"
#include "glew.h"
#include "freeglut.h"
#include "../includes/CString.h"


//----------------------------------------------
//
//----------------------------------------------
Shader::Shader( const char* vertexShaderPath, const char* fragmentShaderPath, const char* geometryShaderPath, const char* tesselationShaderPath )
{
    bool isGeometry = false;

    if( vertexShaderPath == NULL || fragmentShaderPath == NULL ) return;

    CString content = CString::Read( vertexShaderPath );
    m_vShader = CreateShader( content, GL_VERTEX_SHADER );
 
    content = CString::Read( fragmentShaderPath );
    m_fShader = CreateShader( content, GL_FRAGMENT_SHADER );

    if( geometryShaderPath )
    {
        isGeometry = true;
        content = CString::Read( geometryShaderPath );
        m_gShader = CreateShader( content, GL_GEOMETRY_SHADER );
        
    }
    else if( tesselationShaderPath )
    {
        return;
    }

    m_program = glCreateProgram();

    glAttachShader( m_program, m_vShader );
    glAttachShader( m_program, m_fShader );

    if( m_gShader != 0 ) glAttachShader( m_program, m_gShader );

    GLint result = 0;
    char log[ 1024 ];

    glLinkProgram ( m_program );
    glGetProgramiv( m_program, GL_LINK_STATUS, &result );

    if( result == GL_FALSE )
    {
        glGetProgramInfoLog( m_program, sizeof( log ), NULL, log );
        glDeleteProgram    ( m_program );
    }

}
//----------------------------------------------
//
//----------------------------------------------
Shader::~Shader()
{
    glDeleteShader( m_vShader );
    glDeleteShader( m_fShader );
    if( m_gShader != 0 ) glDeleteShader( m_vShader );

    glDeleteProgram( m_program );
}
//----------------------------------------------
//
//----------------------------------------------
void Shader::ActiveProgram()
{
    glUseProgram( m_program );
}
//----------------------------------------------
//
//----------------------------------------------
void Shader::DescActiveProgram()
{
    glUseProgram( 0 );
}
//----------------------------------------------
//
//----------------------------------------------
uint32 Shader::CreateShader( const CString& source, int type )
{
    GLuint      shader = 0;
    GLint       error = 0;
    const char* string = source.ToCString();
    char        log[ 1024 ];

    shader = glCreateShader( type );
    glShaderSource( shader, 1, &string, NULL );
    glCompileShader( shader );
    glGetShaderiv( shader, GL_COMPILE_STATUS, &error );

    if( error == GL_FALSE )
    {
        glGetShaderInfoLog( shader, sizeof( log ), NULL, ( GLchar* ) log );
        glDeleteShader( shader );

        return 0;
    }

    return shader;
}