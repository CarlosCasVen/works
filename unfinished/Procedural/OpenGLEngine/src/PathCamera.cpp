#include "../includes/PathCamera.h"


//---------------------------------
//
//---------------------------------
PathCamera::PathCamera( Spline * spline ) : Camera()
{
    m_t = 0;
	m_path = spline->GenerateSamples(100);
    m_isLooping = false;
    m_pause = false;

}
//---------------------------------
//
//---------------------------------
PathCamera::~PathCamera()
{

}
//---------------------------------
//
//---------------------------------
void PathCamera::Update( float elapsed )
{
    if( m_pause ) return;

	SetPosition(m_path[m_t]);
	SetUsesTarget(true);

	uint32 lookDistance = 1;
	uint32 lookAtPathPoint = m_t;

	lookAtPathPoint += lookDistance;
	if (lookAtPathPoint + lookDistance > m_path.Size())
		lookAtPathPoint -= m_path.Size();

	GetTarget() = m_path[lookAtPathPoint];

	if (m_t < m_path.Size())
		m_t++;
	else
		m_t = 0;
}
//---------------------------------
//
//---------------------------------
void PathCamera::SetTime( uint32 t )
{
    m_t = t;
}
//---------------------------------
//
//---------------------------------
void PathCamera::Pause()
{
    m_pause = true;
}
//---------------------------------
//
//---------------------------------
void PathCamera::Resume()
{
    m_pause = false;
}
//---------------------------------
//
//---------------------------------
void PathCamera::SetLoop( bool looping )
{
    m_isLooping = looping;
}