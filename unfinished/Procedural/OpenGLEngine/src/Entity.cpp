#include "../includes/Entity.h"
#include "../includes/Matrix4.h"
#include "../includes/Renderer.h"

//---------------------------------
//
//---------------------------------
const Vector3& Entity::GetPosition() const
{
    return m_position;
}
//---------------------------------
//
//---------------------------------
Vector3& Entity::GetPosition()
{
    return m_position;
}
//---------------------------------
//
//---------------------------------
const Quat& Entity::GetRotation() const
{
    return m_rotation;
}
//---------------------------------
//
//---------------------------------
Quat& Entity::GetRotation()
{
    return m_rotation;
}
//---------------------------------
//
//---------------------------------
const Vector3& Entity::GetScale() const
{
    return m_scale;
}
//---------------------------------
//
//---------------------------------
Vector3& Entity::GetScale()
{
    return m_scale;
}
//---------------------------------
//
//---------------------------------
void Entity::Move( const Vector3& speed )
{
    m_position += m_rotation * speed;
}
//---------------------------------
//
//---------------------------------
void Entity::Update( float elapsed )
{

}
//---------------------------------
//
//---------------------------------
void Entity::Render()
{
}
//---------------------------------
//
//---------------------------------
Entity::Entity()
{
    m_scale = Vector3( 1, 1, 1 );
    m_typeOfDraw  = GL_TRIANGLES;
}
//---------------------------------
//
//---------------------------------
Entity::~Entity()
{

}
//---------------------------------
//
//---------------------------------
void Entity::SetPosition( Vector3 pos )
{
    m_position = pos;
}
//---------------------------------
//
//---------------------------------
Matrix4 Entity::GetMoldeMatrix()
{
    Matrix4 model;

    model.Translate( m_position );
    model.Rotate( m_rotation.Axis() );
    model.Scale( m_scale );

    return model;
}
//---------------------------------
//
//---------------------------------
void Entity::SetTypeOfDrawing( int type )
{
    m_typeOfDraw = type;
}
