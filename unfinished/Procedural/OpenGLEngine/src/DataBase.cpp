#include "../includes/DataBase.h"
#include "../includes/Renderer.h"
#include "../includes/IScreen.h"
#include "glew.h"
#include "freeglut.h"
#include "../includes/Locations.h"
#include "../includes/ProceduralCilinder.h"
#include "../includes/ProceduralCilinderSpline.h"
#include "../includes/Cell.h"
#include "../includes/SplineEntity.h"
#include "../includes/Edge.h"
#include "../includes/VectorEntity.h"


#define PLANE_WIDTH 4
#define PLANE_HEIGHT 50
#define PLANE_OFFSET 0.5

#define CILINDER_WIDTH  2.0f
#define CILINDER_HEIGHT 2.0f
#define RADIUS 1.0f


DataBase* DataBase::m_dataBase = NULL;


//----------------------------------------------
//
//----------------------------------------------
DataBase* DataBase::Instance()
{
    if( !m_dataBase )
    {
        m_dataBase = new DataBase();
    }
    return m_dataBase;
}
//----------------------------------------------
//
//----------------------------------------------
bool DataBase::Init()
{
    uint16 width,height;

    Renderer& render = Renderer::Instance();

    IScreen::Instance().GetWindowSize( &width, &height );

    //configuracion de la luz
    m_light = new Light();
    m_light->SetType( DIRECTIONAL_LIGHT );
    m_light->SetColor( 1.0f, 1.0f, 1.0f );
    m_light->SetAttenuation( 0.0f );
    m_light->SetPosition( Vector3( 0.0f, -1.0f, -1.0f ) );


    Spline* m_spline = new Spline();
    m_spline->AddPoint(Vector3(   8,    1,  -40));
    m_spline->AddPoint(Vector3(   0,    0,    0));
    m_spline->AddPoint(Vector3(  14,    9,    7));
    m_spline->AddPoint(Vector3(  32,   14,    7));
    m_spline->AddPoint(Vector3(  48,   11,   -4));
    m_spline->AddPoint(Vector3(  51,   16,  -26));
    m_spline->AddPoint(Vector3(  38,   29,  -45));
    m_spline->AddPoint(Vector3(  20,   50,  -52));
    m_spline->AddPoint(Vector3( -10,   66,  -45));
    m_spline->AddPoint(Vector3( -43,   73,  -20));
    m_spline->AddPoint(Vector3( -71,   43,  -20));
    m_spline->AddPoint(Vector3(-108,   35,  -64));
    m_spline->AddPoint(Vector3( -84,   24, -130));
    m_spline->AddPoint(Vector3(  17,   13, -176));
    m_spline->AddPoint(Vector3(  76,   60, -140));
    m_spline->AddPoint(Vector3( 148,  105,  -38));
    m_spline->AddPoint(Vector3( 119,  153,  106));
    m_spline->AddPoint(Vector3(  64,   84,  132));
    m_spline->AddPoint(Vector3( -72,   84,  188));
    m_spline->AddPoint(Vector3(-150,   84,  297));
    m_spline->AddPoint(Vector3( -14,   84,  393));
    m_spline->AddPoint(Vector3( 104,   23,  307));
    m_spline->AddPoint(Vector3( 236,  103,  131));
    m_spline->AddPoint(Vector3( 273,   19,  -36));
    m_spline->AddPoint(Vector3( 245,   80, -184));
    m_spline->AddPoint(Vector3( 179,   80, -180));
    m_spline->AddPoint(Vector3( 139,   17,  -66));
    m_spline->AddPoint(Vector3(  56,    1,   57));
    m_spline->AddPoint(Vector3(  22,    1,   80));
    m_spline->AddPoint(Vector3( -30,    1,   80));
    m_spline->AddPoint(Vector3( -68,   13,   50));
    m_spline->AddPoint(Vector3( -69,    1,   -8));
    m_spline->AddPoint(Vector3( -33,    1,  -39));
    m_spline->AddPoint(Vector3(   8,    1,  -40));
    m_spline->AddPoint(Vector3(   0,    0,    0));
    m_spline->AddPoint(Vector3(  14,    9,    7));

    /*m_spline->AddPoint( Vector3( -8, -40, -40 ) );
    m_spline->AddPoint( Vector3( 0, 0, 0 ) );
    m_spline->AddPoint( Vector3( -20, 40, 40 ) );
    m_spline->AddPoint( Vector3( -40, 80, 80 ) );
    m_spline->AddPoint( Vector3( -80, 80, 80 ) );
    m_spline->AddPoint( Vector3( -100, 40, 40 ) );
    m_spline->AddPoint( Vector3( -80, 0, 0 ) );
    m_spline->AddPoint( Vector3( -40, -60, -60 ) );*/

    // Tu, sabes que las venas con diferentes formas, no redondas me refiero, es un extra, no�? XDDDDD
    //si pero no esto que esta pasando que se jode la maya y ya no son las rotaciones creo que hay algo raro en los puntos
    // Pero los puntos estan sobre el spline, no tienen rotacion de hecho es que se ve en el path, a
    //cuando se crea la interpolacion se crean puntos la cosa es que para sacar la rotacion se hace a partir de la tangente a ese punto
    //eso se obtiene con ptoSiguiente - ptoActual
    //creo que ahi es donde hay algo raro porque revientay lo he probado unidimensionalmente y va

    
    Entity* entity = new ProceduralCilinderSpline( 4, 10, 4, 4., m_spline );
  //  Entity* entity = new ProceduralCilinder( 4, 4, 50, 4, 2. );
    entity->Init();
    m_entities.Add( entity );

    entity = new Edge( Vector3( 1,0,0), Vector3(1,0,0) );
    entity->Init();
    m_entities.Add( entity );

    entity = new Edge( Vector3( 0, 1, 0 ), Vector3( 0, 1, 0 ) );
    entity->Init();
    m_entities.Add( entity );

    entity = new Edge( Vector3( 0, 0, 1 ), Vector3( 0, 0, 1 ) );
    entity->Init();
    m_entities.Add( entity );

    entity = new SplineEntity( m_spline );
    entity->Init();
    m_entities.Add( entity );

 /*   Array<Vector3> samples = m_spline->GenerateSamples( 20 );
    entity = new Edge( samples[ 1 ] - samples[0], Vector3( 0, 1, 1 ) );
    entity->Init();
    m_entities.Add( entity );*/

   /* entity = new VectorEntity( samples[ 0 ], samples[ 20 ] - samples[ 0 ], Vector3( 1, 0, 1 ) );
    entity->Init();
    m_entities.Add( entity );
    
    Vector3 d = samples[ 20 ] - samples[ 0 ];
    Vector3 z = Vector3( 0, 0, -1 );
    */
  /*  Matrix4 m;
    m.LookAt(Vector3(0, 0, 1), Vector3(0, 0, 0) + d, Vector3(0,1,0));

    entity = new VectorEntity( samples[ 0 ], m * Vector3( 0,0,1), Vector3( 0, 1, 1 ) );
    entity->Init();
    m_entities.Add( entity );*/

	//config de la camara
	m_camera = new PathCamera( m_spline );
	//m_camera->GetPosition() = Vector3(.0f, .0f, 3.0f);
	
    //m_camera = new Camera();

    m_camera->SetPosition( Vector3( 1, 1, 10 ));
    m_camera->GetTarget() = Vector3( 0,0,0);   
    m_camera->SetViewport(0, 0, width, height);
	m_camera->SetPerspective(45, float(width) / float(height), 0.001f, 1000.0f);
	m_camera->SetUsesTarget(true);

    return true;
}
//----------------------------------------------
//
//----------------------------------------------
void DataBase::End()
{
    
    delete m_camera;
    
    for( int index = m_entities.Size() - 1; index >= 0; index-- )
    {
        m_entities[index]->End();
        delete m_entities[ index ];
    }


    delete m_dataBase;
}
//----------------------------------------------
//
//----------------------------------------------
void DataBase::Update( float elapsed )
{
    m_camera->Update( elapsed );

    for( unsigned int index = 0; index < m_entities.Size(); index++ )
    {
        m_entities[ index ]->Update( elapsed );
    }

    m_light->Update( elapsed );
}
//----------------------------------------------
//
//----------------------------------------------
void DataBase::Render()
{
    for( unsigned int index = 0; index < m_entities.Size(); index++ )
    {
        m_entities[index]->Render();       
    }
}
//----------------------------------------------
//
//----------------------------------------------
void DataBase::PrepareLights()
{
    Renderer& render = Renderer::Instance();
    float lightPos[ 4 ] = {
        m_light->GetPosition().X(),
        m_light->GetPosition().Y(),
        m_light->GetPosition().Z(),
        static_cast<float>( m_light->GetType() )
    };

    render.SetUniformVec3  ( 3, m_camera->GetPosition() );
    render.SetUniformVec4  ( 4, lightPos );
    render.SetUniformFloat ( 5, m_light->GetAttenuation() );
    render.SetUniformVec3  ( 6, Vector3(m_light->GetRed(), m_light->GetGreen(), m_light->GetBlue() ) );
    render.SetUniformVec3  ( 7, Vector3( 0.2f, 0.2f, 0.2f ) );
    render.SetUniformInt   ( 8, 16 );

}
//----------------------------------------------
//
//----------------------------------------------
DataBase::DataBase()
{
    
}
//----------------------------------------------
//
//----------------------------------------------
DataBase::~DataBase()
{
}
//----------------------------------------------
//
//----------------------------------------------
Camera* DataBase::GetCamera()
{
    return m_camera;
}
//----------------------------------------------
//
//----------------------------------------------
Array<Entity*> DataBase::GetEntities()
{
    return m_entities;
}