#include "../includes/ProceduralCilinderSpline.h"
#include "../includes/Renderer.h"
#include "../includes/Vertex.h"
#include "../includes/MathFunctions.h"
#include "../includes/PerlinNoise.h"
#include "../includes/DataBase.h"
#include "../includes/Renderer.h"
#include "../includes/MathFunctions.h"
#include "../includes/VectorEntity.h"
#include <stdio.h>
#include <stddef.h>
#include <time.h>

#define TEXTURE_SIZE 512

#define VERTEX_SHADER_PATH "data/vshader.txt"
#define FRAGMENT_SHADER_PATH "data/fshader.txt"


Array<float> debugArray;
Array<int> debugIndex;
int vao,vbo,ebo;
//---------------------------------
//
//---------------------------------
ProceduralCilinderSpline::ProceduralCilinderSpline( uint16 numHeightDivisions, uint16 numWidthDivisions, float width, float radius, Spline* spline ) : Entity()
{
    m_numHeightDivisions = numHeightDivisions;
    m_numWidthDivisions = numWidthDivisions;
    m_offsetWidth = width / static_cast<float>( numWidthDivisions );
    m_radius = radius;
    m_spline = spline;

    m_shader = new Shader( VERTEX_SHADER_PATH, FRAGMENT_SHADER_PATH );
}
//---------------------------------
//
//---------------------------------
ProceduralCilinderSpline::~ProceduralCilinderSpline()
{
    delete m_shader;
}
//---------------------------------
//
//---------------------------------
bool ProceduralCilinderSpline::Init()
{
    Array<Vertex> dataMesh;
    Array<unsigned int> indexes;
    Array<Vector3> samples = m_spline->GenerateSamples( m_numHeightDivisions );
    
    //generacion de vertices
    float z = 0.0f;
    float uOffset = 1.0f / ( m_numHeightDivisions - 1 ); // Cierra el circulo con los puntos (en un plano) y comprobamos con las rotaciones, como hace, porque si est�n mal ahi en un plano en 2 d (quitada la rotacion en Z).. no se si me entiendes
    float vOffset = 1.0f / ( m_numWidthDivisions - 1 );
    float angleStep = 360.0f / static_cast<float>( m_numWidthDivisions - 1 );
 
    //Calculo del angulo
    Vector3 zEdge = Vector3( 0, 0, 1 );
    float yRot;
    float xRot;
    Matrix4 MatrixRotY;

    for( unsigned int r = 0; r < samples.Size(); r++ )
    {
        Vector3 slicePosition = samples[ r     ];
        Vector3 nextSlice     = samples[ r + 1 ];

        Vector3 dir = samples[ ( r + 1 ) % samples.Size() ] - samples[ r ];
        dir = dir.Normalized();

        yRot = AngleBetweenVectors( MatrixRotY * zEdge, dir, XZ );

        MatrixRotY.Rotate( RotAxis( yRot, MatrixRotY * Vector3( 0, 1, 0 ) ) );

        xRot = AngleBetweenVectors( MatrixRotY * zEdge, dir, YZ );

        MatrixRotY.Rotate(RotAxis(xRot, MatrixRotY * Vector3( 0., 1., 0. ) ) );

        printf( "Direccion: %f, %f, %f; %f; %f  \n", dir.x, dir.y, dir.z, yRot, xRot );

        
        for( unsigned int c = 0; c < m_numWidthDivisions; c++ )
        {
            float x = static_cast<float>( DegCos( static_cast<float>( ( c % ( m_numWidthDivisions - 1 ) ) * angleStep ) ) * m_radius );
            float y = static_cast<float>( DegSin( static_cast<float>( ( c % ( m_numWidthDivisions - 1 ) ) * angleStep ) ) * m_radius );

            Vector3 circlePos = Vector3( x, y, 0 );
            Vector3 vertex    = slicePosition + MatrixRotY * circlePos;

            dataMesh.Add(
                        Vertex(
                                vertex,
                                uOffset * r,
                                vOffset * c,
                                vertex * -1
                        )
                );
            
        }
    }

    //creacion de indices
    for( unsigned int r = 0; r < ( unsigned int ) samples.Size() - 1; r++ )
    {
        for( unsigned int c = 0; c < ( unsigned int ) m_numWidthDivisions - 1; c++ )
        {
            uint16 currentRow = ( uint16 ) r * ( m_numWidthDivisions );
            uint16 nextRow = ( uint16 ) ( r + 1 ) * ( m_numWidthDivisions );

            //triangulo 1
            indexes.Add( currentRow + c );
            indexes.Add( nextRow + c );
            indexes.Add( currentRow + c + 1 );

            //segundo
            indexes.Add( currentRow + c + 1 );
            indexes.Add( nextRow + c );
            indexes.Add( nextRow + c + 1 );

           /* printf( "Indices triangle 1: %d, %d, %d\n", currentRow + c, nextRow + c, currentRow + c + 1 );
            printf( "Indices triangle 2: %d, %d, %d\n", currentRow + c + 1, nextRow + c, nextRow + c + 1 );*/
        }
    }

    PerlinNoise n( 512 );
    float* matrix = new float[ 512 * 512 * 3 ];


    for( int i = 0; i < 512; i++ ) {
        for( int j = 0; j < 512; j++ ) {
            for( int c = 0; c < 3; c++ )
                matrix[ ( j * 512 + i ) * 3 + c ] = n.get( 10 * ( float ) j / ( ( float ) 512.0f ), 10 * ( float ) i / ( ( float ) 512.0f ), 0. );
        }
    }

    Renderer& render = Renderer::Instance();

    //GENERACION DE TEXTURA
    m_textureDiffuse = render.CreateTexture();
    render.BindTexture2D( m_textureDiffuse );
    render.ConfigTexture2D();

    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB, 512, 512, 0, GL_RGB, GL_FLOAT, matrix );

    render.BindTexture2D( m_textureDiffuse );

    //GENERACION DE MALLA 

    m_vao = render.CreateVAO();
    m_vbo = render.CreateVBO();
    m_ebo = render.CreateVBO();

    render.BindVAO( m_vao );
    render.BindVBO( m_vbo, GL_ARRAY_BUFFER );

    glBufferData( GL_ARRAY_BUFFER, sizeof( Vertex ) * dataMesh.Size(), &dataMesh[ 0 ], GL_STATIC_DRAW );

    //enlace de los vertices
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, sizeof( Vertex ), ( const void* ) offsetof( Vertex, m_position ) );
    glEnableVertexAttribArray( 0 );

    //enlace de las normales
    glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, sizeof( Vertex ), ( const void* ) offsetof( Vertex, m_normal ) );
    glEnableVertexAttribArray( 1 );

    //enlace de las texcoords
    glVertexAttribPointer( 2, 2, GL_FLOAT, GL_FALSE, sizeof( Vertex ), ( const void* ) offsetof( Vertex, m_u ) );
    glEnableVertexAttribArray( 2 );

    render.BindVBO( m_ebo, GL_ELEMENT_ARRAY_BUFFER );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( unsigned int ) * indexes.Size(), &indexes[ 0 ], GL_STATIC_DRAW );

    render.BindVBO( 0, GL_ELEMENT_ARRAY_BUFFER );
    render.BindVBO( 0, GL_ARRAY_BUFFER );
    render.BindVAO( 0 );

    m_nIndices = ( uint16 ) indexes.Size();

    ///DEBUG//////
    vao = render.CreateVAO();
    vbo = render.CreateVBO();

    render.BindVAO(vao);
    render.BindVBO( vbo, GL_ARRAY_BUFFER);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * debugArray.Size(), &debugArray[0], GL_STATIC_DRAW );
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray( 0 );
    render.BindVAO( 0 );
    render.BindVBO( 0, GL_ELEMENT_ARRAY_BUFFER );
    return true;
}
//---------------------------------
//
//---------------------------------
void ProceduralCilinderSpline::End()
{
    Renderer::Instance().FreeVAOBuffers( m_vao );
    Renderer::Instance().FreeVBOBuffers( m_ebo );
    Renderer::Instance().FreeVBOBuffers( m_vbo );
    Renderer::Instance().FreeTexture( m_textureDiffuse );
}
//---------------------------------
//
//---------------------------------
void ProceduralCilinderSpline::Render()
{
    m_shader->ActiveProgram();

    Matrix4 projection = DataBase::Instance()->GetCamera()->GetProjection();
    Matrix4 view = DataBase::Instance()->GetCamera()->GetView();


    Renderer::Instance().ActivateTexture( 0, m_textureDiffuse, 0 );
    Matrix4 model = GetMoldeMatrix();
    Matrix4 mvp = projection * view * model;

    Renderer::Instance().SetUniformMatrix( 0, model );
    Renderer::Instance().SetUniformMatrix( 1, mvp );
    Renderer::Instance().SetUniformInt( 8, 16 );

    Renderer::Instance().BindVAO( m_vao );
    Renderer::Instance().BindVBO( m_ebo, GL_ELEMENT_ARRAY_BUFFER );

    glDrawElements( m_typeOfDraw, m_nIndices, GL_UNSIGNED_INT, 0 );

    Renderer::Instance().BindVAO( 0 );
    Renderer::Instance().BindVBO( 0, GL_ELEMENT_ARRAY_BUFFER );
    Renderer::Instance().BindTexture2D( 0 );

    m_shader->DescActiveProgram();
}