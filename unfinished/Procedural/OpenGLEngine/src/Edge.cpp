#include "../includes/Edge.h"
#include "../includes/Renderer.h"
#include "../includes/Vertex.h"
#include "../includes/MathFunctions.h"
#include "../includes/PerlinNoise.h"
#include "../includes/DataBase.h"
#include "../includes/Renderer.h"
#include "../includes/MathFunctions.h"
#include <stdio.h>
#include <stddef.h>
#include <time.h>

#define TEXTURE_SIZE 512

#define VERTEX_SHADER_PATH "data/vEdge.txt"
#define FRAGMENT_SHADER_PATH "data/fEdge.txt"

//---------------------------------
//
//---------------------------------
Edge::Edge( Vector3 dir, Vector3 color, float distance ) : Entity()
{
   m_dir = dir;
   m_distance = distance;
   m_color = color;

    m_shader = new Shader( VERTEX_SHADER_PATH, FRAGMENT_SHADER_PATH );
}
//---------------------------------
//
//---------------------------------
Edge::~Edge()
{
    delete m_shader;
}
//---------------------------------
//
//---------------------------------
bool Edge::Init()
{
    Renderer& render = Renderer::Instance();

    float positions[] = { 0.0, 0.0, 0.0, 
                          m_dir.X() * m_distance, m_dir.Y() * m_distance, m_dir.Z() * m_distance
                        };

    m_vao = render.CreateVAO();
    m_vbo = render.CreateVBO();

    render.BindVAO( m_vao );
    render.BindVBO( m_vbo, GL_ARRAY_BUFFER );

    glBufferData( GL_ARRAY_BUFFER, sizeof( positions), positions, GL_STATIC_DRAW );

    //enlace de los vertices
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray( 0 );

    render.BindVBO( 0, GL_ARRAY_BUFFER );
    render.BindVAO( 0 );

    return true;
}
//---------------------------------
//
//---------------------------------
void Edge::End()
{
    Renderer::Instance().FreeVAOBuffers( m_vao );
    Renderer::Instance().FreeVBOBuffers( m_vbo );
}
//---------------------------------
//
//---------------------------------
void Edge::Render()
{
    m_shader->ActiveProgram();

    Renderer::Instance().BindVAO( m_vao );

    Matrix4 projection = DataBase::Instance()->GetCamera()->GetProjection();
    Matrix4 view = DataBase::Instance()->GetCamera()->GetView();

    Matrix4 model = GetMoldeMatrix();
    Matrix4 mvp = projection * view * model;

    Renderer::Instance().SetUniformMatrix( 0, model );
    Renderer::Instance().SetUniformMatrix( 1, mvp );
    Renderer::Instance().SetUniformVec3( 12, m_color );
    
    Renderer::Instance().BindVAO( m_vao );
    
    glDrawArrays( GL_LINE_STRIP, 0, 2 );

    Renderer::Instance().BindVAO( 0 );

    Renderer::Instance().BindTexture2D( 0 );

    m_shader->DescActiveProgram();
}