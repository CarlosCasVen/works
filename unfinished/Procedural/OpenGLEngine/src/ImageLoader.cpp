#include "../includes/ImageLoader.h"
#include "../includes/CString.h"
#include "../includes/stb_image.c"

extern "C" unsigned char *stbi_load(char const *filename, int *x, int *y, int *comp, int req_comp);
extern "C" void stbi_image_free(void *retval_from_stbi_load);

bool ImageLoader::Load(CString path, uint8 &image_buffer, int32 &width, int32 &height)
{
	int32 image_comp  = 0;
	uint32 new_width  = 0;
	uint32 new_height = 0;

	bool needs_to_be_scaled = false;

	uint8 * image_scaled_buffer;
	uint8 * image_no_scaled_buffer;

	// Carga en buffer de la imagen
	image_no_scaled_buffer = stbi_load(path.ToCString(), &width, &height, &image_comp, 4);

	// Comprobar que ancho y alto no sea potencia de 2
	if (!IsPOT(width) || !IsPOT(height)) 
	{
		needs_to_be_scaled = true;

		// Calculamos el nuevo tama�o POT
		new_width  = (uint32) pow(2, ceil(log(width) / log(2.f)));
		new_height = (uint32) pow(2, ceil(log(height) / log(2.f)));

		// Creamos el nuevo buffer
		uint64 new_image_size = new_width * new_height * 4;
		image_scaled_buffer   = new unsigned char[new_image_size];

		memset(image_scaled_buffer, 0, new_image_size);

		// Copiamos imagen en el nuevo buffer
		for (uint32 y = 0; y < (uint32) height; y++)
		for (uint32 x = 0; x < (uint32) width; x++)
				for (uint8 c = 0; c < 4; c++)
					image_scaled_buffer[(y*new_width + x) * 4 + c] = image_no_scaled_buffer[(y*width + x) * 4 + c];

		image_buffer = *image_scaled_buffer;

		// Liberamos el no_scaled_buffer
		stbi_image_free(image_no_scaled_buffer);
	} 
	else 
	{
		image_buffer = *image_no_scaled_buffer;
	}

	if (needs_to_be_scaled)
	{
		width  = new_width;
		height = new_height;
	}

	return true;
}

bool ImageLoader::IsPOT(int32 x) 
{
	while (!(x & 1))
		x = x >> 1;

	return x == 1;
}