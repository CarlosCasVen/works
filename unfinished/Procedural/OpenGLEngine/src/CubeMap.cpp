#include "../includes/CubeMap.h"
#include "../includes/ProceduralCilinder.h"
#include "../includes/Renderer.h"
#include "../includes/Vertex.h"
#include "../includes/MathFunctions.h"
#include "../includes/PerlinNoise.h"
#include "../includes/DataBase.h"
#include "../includes/Renderer.h"
#include <stdio.h>
#include <stddef.h>
#include <time.h>

#define TEXTURE_SIZE 512

#define VERTEX_SHADER_PATH "data/vCubeMapShader.txt"
#define FRAGMENT_SHADER_PATH "data/fCubeMapShader.txt"

//---------------------------------
//
//---------------------------------
CubeMap::CubeMap() : Entity()
{
    m_shader = new Shader( VERTEX_SHADER_PATH, FRAGMENT_SHADER_PATH );
}
//---------------------------------
//
//---------------------------------
CubeMap::~CubeMap()
{
    delete m_shader;
}
//---------------------------------
//
//---------------------------------
bool CubeMap::Init()
{
    //CREACION DEL CUBO DEL SKYBOX
    m_vao = Renderer::Instance().CreateVAO();
    m_vbo = Renderer::Instance().CreateVBO();

    float cube[] = {
        // Positions          
        -1.0f, 1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,

        -1.0f, -1.0f, 1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,

        1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,

        -1.0f, -1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,

        -1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, -1.0f,

        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f
    };

    Renderer::Instance().BindVAO( m_vao );
    Renderer::Instance().BindVBO( m_vbo, GL_ARRAY_BUFFER );

    glBufferData( GL_ARRAY_BUFFER, sizeof( cube ), cube, GL_STATIC_DRAW );

    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray( m_vao );

    Renderer::Instance().BindVBO( 0, GL_ARRAY_BUFFER );
    Renderer::Instance().BindVAO( 0 );

    return true;
}
//---------------------------------
//
//---------------------------------
void CubeMap::End()
{

}
//---------------------------------
//
//---------------------------------
void CubeMap::Render()
{
    m_shader->ActiveProgram();

    Renderer::Instance().BindVAO( m_vao );
    Renderer::Instance().BindVBO( m_vbo, GL_ARRAY_BUFFER );
    Renderer::Instance().ActivateTexture( 0, m_cubeTexture , 0 );

    Renderer::Instance().SetUniformMatrix(  9, DataBase::Instance()->GetCamera()->GetProjection() );
    Renderer::Instance().SetUniformMatrix( 10, DataBase::Instance()->GetCamera()->GetView()       );

    glDrawArrays( GL_TRIANGLES, 0, 36 );

    Renderer::Instance().BindVBO( 0, GL_ARRAY_BUFFER );
    Renderer::Instance().BindVAO( 0 );

    m_shader->DescActiveProgram();
}