#include "glew.h"
#include "freeglut.h"
#include "../includes/Engine.h"
#include "../includes/Screen.h"
#include "../includes/Renderer.h"
#include "../includes/DataBase.h"
#include "../includes/Camera.h"

//---------------------------------
//
//---------------------------------
void Engine::Init()
{
    
    Renderer::Instance().Init();
    DataBase::Instance()->Init();
    glutMainLoop();
}
//---------------------------------
//
//---------------------------------
void Engine::ReshapeFunc( int width, int height )
{
    IScreen::Instance().SetWindowSize( width, height );
}
//---------------------------------
//
//---------------------------------
void Engine::Display()
{
    //realizar el render de todos los objetos
    Renderer::Instance().DeferredShading();
}
//---------------------------------
//
//---------------------------------
void Engine::MainLoop()
{
    //Update de todos los managers
    IScreen::Instance().Update( Screen::Instance().GetElapsedTime() );
    DataBase::Instance()->Update( IScreen::Instance().GetElapsedTime() );
}
//---------------------------------
//
//---------------------------------
void Engine::End()
{
    //hay que liberar recursos
    DataBase::Instance()->End();
    Renderer::Instance().End();
}
//---------------------------------
//
//---------------------------------
void Engine::Input( unsigned char key, int x, int y  )
{
    Camera* cam = DataBase::Instance()->GetCamera();
    Array<Entity*> entities = DataBase::Instance()->GetEntities();

    switch( key )
    {
        //definicion de movimiento de la camara
    case 'a': cam->GetPosition() += Vector3( -1.0,  0.0, 0.0 ); break;
    case 'd': cam->GetPosition() += Vector3(  1.0,  0.0, 0.0 ); break;
    case 'w': cam->GetPosition() += Vector3(  0.0,  1.0, 0.0 ); break;
    case 's': cam->GetPosition() += Vector3(  0.0, -1.0, 0.0 ); break;
    
        //Definicion del tipo de pintado
    case '1':
    {
        for( unsigned int index = 0; index < entities.Size(); index++ )
        {
            entities[index]->SetTypeOfDrawing( GL_TRIANGLES );
        }
        break;
    }
    case '2':
    {
        for( unsigned int index = 0; index < entities.Size(); index++ )
        {
            entities[ index ]->SetTypeOfDrawing( GL_LINES );
        }
        break;
    }
    case '3':
    {
        for( unsigned int index = 0; index < entities.Size(); index++ )
        {
            entities[ index ]->SetTypeOfDrawing( GL_POINTS );
        }
        break;
    }
    case '4':
    {
        for( unsigned int index = 0; index < entities.Size(); index++ )
        {
            entities[ index ]->SetTypeOfDrawing( GL_LINE_STRIP );
        }
        break;
    }
    }
}