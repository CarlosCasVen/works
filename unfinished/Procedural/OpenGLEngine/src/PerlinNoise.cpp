#include "../includes/PerlinNoise.h"
#include <iostream>
#include <cmath>
#include <random>
#include <algorithm>

PerlinNoise::PerlinNoise(uint32 seed) 
{
	m_permutation.resize(256);
	
	int i = 0;
	for(std::vector<int32>::iterator it = m_permutation.begin(); it != m_permutation.end(); ++it, i++) {
		*it = i;
	}

	std::random_shuffle(m_permutation.begin(), m_permutation.end());

	m_permutation.insert(m_permutation.end(), m_permutation.begin(), m_permutation.end());
}

float PerlinNoise::get(float x, float y, float z) 
{
	int X = (int) floor(x) & 255;
	int Y = (int) floor(y) & 255;
	int Z = (int) floor(z) & 255;

	int A  = m_permutation[X]     + Y;
	int AA = m_permutation[A]     + Z;
	int AB = m_permutation[A + 1] + Z;
	int B  = m_permutation[X + 1] + Y;
	int BA = m_permutation[B]     + Z;
	int BB = m_permutation[B + 1] + Z;

	x -= floor(x);
	y -= floor(y);
	z -= floor(z);

	float u = pow(x, 3) * (x * (6 * x - 15) + 10);
	float v = pow(y, 3) * (y * (6 * y - 15) + 10);
	float w = pow(z, 3) * (z * (6 * z - 15) + 10);

	float res = 
		lerp(w, 
			lerp(v, 
				lerp(u, 
					 grad(m_permutation[AA], x, y, z), 
					 grad(m_permutation[BA], x-1, y, z)
					), 
				lerp(u, 
					 grad(m_permutation[AB], x, y-1, z), 
					 grad(m_permutation[BB], x-1, y-1, z))
				),	
			lerp(v, 
				lerp(u, 
					 grad(m_permutation[AA+1], x, y, z-1), 
					 grad(m_permutation[BA+1], x-1, y, z-1)
					), 
				lerp(u, 
					 grad(m_permutation[AB+1], x, y-1, z-1),	
					 grad(m_permutation[BB+1], x-1, y-1, z-1)
					)
				)
			);
	
	return (res + 1.0)/2.0;
}

float PerlinNoise::lerp(float t, float a, float b) 
{ 
	return a + t * (b - a); 
}

float PerlinNoise::grad(int hash, float x, float y, float z)
{
	int h = hash & 15;

	float u = h < 8 ? x : y;
	float v = h < 4 ? y : h == 12 || h == 14 ? x : z;

	return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
}

