#include "../includes/Camera.h"
#include "../includes/Renderer.h"
#include "../includes/IScreen.h"




//---------------------------------
//
//---------------------------------
int32 Camera::GetViewportX() const
{
    return m_vx;
}
//---------------------------------
//
//---------------------------------
int32 Camera::GetViewportY() const
{
    return m_vy;
}
//---------------------------------
//
//---------------------------------
uint16 Camera::GetViewportWidth() const
{
    return m_vw;
}
//---------------------------------
//
//---------------------------------
uint16 Camera::GetViewportHeight() const
{
    return m_vh;
}
//---------------------------------
//
//---------------------------------
void Camera::SetViewport( int32 x, int32 y, uint16 w, uint16 h )
{
    m_vx = x;
    m_vy = y;
    m_vw = w;
    m_vh = h;
}
//---------------------------------
//
//---------------------------------
void Camera::SetOrtho( float left, float right, float bottom, float top, float n, float f )
{
    m_projMatrix.SetIdentity();
    m_projMatrix.SetOrtho( left, right, bottom, top, n, f );
}
//---------------------------------
//
//---------------------------------
void Camera::SetFrustum( float left, float right, float bottom, float top, float n, float f )
{
    m_projMatrix.SetIdentity();
    m_projMatrix.SetFrustum( left, right, bottom, top, n, f );
}
//---------------------------------
//
//---------------------------------
void Camera::SetPerspective( float fov, float ratio, float n, float f )
{
    m_projMatrix.SetIdentity();
    m_projMatrix.SetPerspective( fov, ratio, n, f );
}
//---------------------------------
//
//---------------------------------
bool Camera::UsesTarget() const
{
    return m_usesTarget;
}
//---------------------------------
//
//---------------------------------
void Camera::SetUsesTarget( bool usesTarget )
{
    m_usesTarget = usesTarget;
}
//---------------------------------
//
//---------------------------------
const Vector3& Camera::GetTarget() const
{
    return m_target;
}
//---------------------------------
//
//---------------------------------
Vector3& Camera::GetTarget()
{
    return m_target;
}
//---------------------------------
//
//---------------------------------
const Matrix4& Camera::GetProjection() const
{
    return m_projMatrix;
}
//---------------------------------
//
//---------------------------------
const Matrix4& Camera::GetView()
{
    m_viewMatrix.SetIdentity();

    Vector3 trgt = ( m_usesTarget ) ? m_target : GetPosition() + GetRotation() * Vector3( 0, 0, -1 );

    m_viewMatrix.LookAt( GetPosition(), trgt, Vector3( 0, 1, 0 ) );

    return m_viewMatrix;
}
//---------------------------------
//
//---------------------------------
void Camera::Update( float elapsed )
{

}
//---------------------------------
//
//---------------------------------
void Camera::Render()
{
    m_viewMatrix.SetIdentity();

    Vector3 trgt = ( m_usesTarget ) ? m_target : GetPosition() + GetRotation() * Vector3( 0, 0, -1 );

    m_viewMatrix.LookAt( GetPosition(), trgt, Vector3( 0, 1, 0 ) );

   /* Renderer::Instance()->SetViewport( vx, vy, vw, vh );
    
    Vector3 pos = GetPosition();
    Renderer::Instance()->SetEyePos( pos.X(), pos.Y(), pos.Z() );*/
}
//---------------------------------
//
//---------------------------------
Camera::Camera()
{
    m_vx = m_vy = 0;
    m_usesTarget = false;
    
    IScreen::Instance().GetWindowSize( &m_vw, &m_vh );
}
//---------------------------------
//
//---------------------------------
Camera::~Camera()
{
}

////---------------------------------------------------------------------------////
//---------------------------------
//
//---------------------------------
bool Camera::Init()
{
    return true;
}
//---------------------------------
//
//---------------------------------
void Camera::End()
{
   
}
//---------------------------------
//
//---------------------------------
uint32 Camera::GetVAO()
{
    return 0;
}
//---------------------------------
//
//---------------------------------
uint32 Camera::GetEBO()
{
    return 0;
}

uint32 Camera::GetNumIndices()
{
    return 0;
}

void Camera::ActiveDiffuse()
{

}