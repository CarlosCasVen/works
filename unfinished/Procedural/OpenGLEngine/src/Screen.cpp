#include <stdio.h>
#include "glew.h"
#include "freeglut.h"
#include "../includes/Screen.h"

#define DEFAULT_WIDTH  800
#define DEFAULT_HEIGHT 600
#define DEFAULT_NAME "default"

static IScreen* m_screen = NULL;
//----------------------------------------------
//
//----------------------------------------------
IScreen& IScreen::Instance()
{
    if( !m_screen )
    {
        m_screen = new Screen();
    }

    return *m_screen;
}
//----------------------------------------------
//
//----------------------------------------------
bool Screen::Init()
{
    m_isOpened  = false;
   
    return true;
}
//----------------------------------------------
//
//----------------------------------------------
void Screen::End()
{
    if( m_screen )
    {
        delete m_screen;
    }

    m_screen = NULL;
}
//----------------------------------------------
//
//----------------------------------------------
void Screen::SetFullscreen( bool fullscreen )
{
    fullscreen ? glutFullScreen() : glutLeaveFullScreen();
}
//----------------------------------------------
//
//----------------------------------------------
void Screen::SetWindowName(const char* name)
{
    glutSetWindowTitle( name );
}
//----------------------------------------------
//
//----------------------------------------------
void Screen::SetWindowSize(int16 width, int16 height)
{
    glutReshapeWindow( width, height );
}
//----------------------------------------------
//
//----------------------------------------------
float Screen::GetElapsedTime() const
{
    if ( m_milisecondsFromInit == glutGet(GLUT_ELAPSED_TIME) )
        return m_elapsedTime;
    return ( static_cast<float>(glutGet(GLUT_ELAPSED_TIME)) - static_cast<float>(m_milisecondsFromInit ) ) * 0.001f;
    
    return 0;
}
//----------------------------------------------
//
//----------------------------------------------
void Screen::GetWindowSize(uint16* width, uint16* height) const
{
    *width  = static_cast<uint16>( glutGet( GLUT_WINDOW_WIDTH  ) );
    *height = static_cast<uint16>( glutGet( GLUT_WINDOW_HEIGHT ) );
}
//----------------------------------------------
//
//----------------------------------------------
void Screen::Update( float elapsed )
{
    m_elapsedTime           = ( static_cast<float>(glutGet(GLUT_ELAPSED_TIME)) - static_cast<float>(m_milisecondsFromInit)) * 0.001f;
    m_milisecondsFromInit   = glutGet(GLUT_ELAPSED_TIME);
}
//----------------------------------------------
//
//----------------------------------------------
void Screen::Clear( uint8 r, uint8 g, uint8 b, uint8 a )
{
    glClearColor( r, g, b, a );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
}
//----------------------------------------------
//
//----------------------------------------------
Screen::Screen()
{
    m_isOpened              = false;
    m_milisecondsFromInit   = 0;
    m_elapsedTime           = .0f;
}
//----------------------------------------------
//
//----------------------------------------------
Screen::~Screen()
{
}

