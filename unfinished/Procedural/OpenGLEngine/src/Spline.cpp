#include "../includes/Spline.h"

//----------------------------------------------
//
//----------------------------------------------
Spline::Spline( float r, float dt )
{
    m_dt = dt;
    m_r = r;
}
//----------------------------------------------
//
//----------------------------------------------
Spline::~Spline()
{

}
//----------------------------------------------
//
//----------------------------------------------
void Spline::SetDeltaTime( float dt )
{
    m_dt = dt;
}
//----------------------------------------------
//
//----------------------------------------------
void Spline::SetBlending( float r )
{
    m_r = r;
}
//----------------------------------------------
//
//----------------------------------------------
void Spline::AddPoint( Vector3 point )
{
    m_points.Add( point / 5 );

    m_dt = ( float ) 1 / ( float ) m_points.Size();
}
//----------------------------------------------
//
//----------------------------------------------
Vector3 Spline::GetPoint( int point )
{
    return m_points[ point ];
}
//----------------------------------------------
//
//----------------------------------------------
Array<Vector3> Spline::GetPoints()
{
    return m_points;
}
//----------------------------------------------
//
//----------------------------------------------
Vector3 Spline::Get( float t )
{
    int32 p = ( int32 ) ( t / m_dt );

    uint32 p0 = SafeLimit( p - 1 );
    uint32 p1 = SafeLimit( p );
    uint32 p2 = SafeLimit( p + 1 );
    uint32 p3 = SafeLimit( p + 2 );

    float lt = ( t - m_dt * ( float ) p ) / m_dt;

    return Interpolate( lt, m_points[ p0 ], m_points[ p1 ], m_points[ p2 ], m_points[ p3 ] );
}

//----------------------------------------------
//
//----------------------------------------------
Array<Vector3> Spline::GenerateSamples(uint32 samples)
{
	Array<Vector3> curve;

	if (m_points.Size() < 4)
		return curve;

	for (int n = 1; n < m_points.Size() - 2; n++)
		for (int i = 0; i < samples; i++)
			curve.Add(
				Interpolate(
					(1.f / samples) * i, 
					m_points[n - 1], 
					m_points[n], 
					m_points[n + 1], 
					m_points[n + 2]
				)
			);

		curve.Add(m_points[m_points.Size() - 2]);

	return curve;
}

//----------------------------------------------
//
//----------------------------------------------
Vector3 Spline::Interpolate( float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3 )
{
	float t2 = t * t;
	float t3 = t2 * t;

	float b1 = m_r * (-t3 + 2 * t2 - t);
	float b2 = m_r * (3 * t3 - 5 * t2 + 2);
	float b3 = m_r * (-3 * t3 + 4 * t2 + t);
	float b4 = m_r * (t3 - t2);

	return p0*b1 + p1*b2 + p2*b3 + p3*b4;
}
//----------------------------------------------
//
//----------------------------------------------
uint32 Spline::SafeLimit( int32 point )
{
    int index = 0;
    
    if( point < 0 )
        index = 0;
    else if( point > m_points.Size() )
        index = m_points.Size() - 1;
    else
        index = point;

    return index;
}