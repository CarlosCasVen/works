#include "../includes/Voronoi.h"
#include "../includes/Vector3.h"
#include <stdlib.h>
#include <time.h>


float * Voronoi::Generate(int32 seed, int32 size, int32 N)
{
	//Matriz resultante (size x size x 3(RGB))
	float * matrix = new float[size * size * 3];
	  
	Vector3 **grid = new Vector3*[N];
	for (int32 k = 0; k < N; k++)
	{
		grid[k] = new Vector3[N];
	}

	//N�mero de p�xeles de cada partici�n: pixels x pixels (tama�o total entre n�mero de particiones)
	int32 pixels = size / N;

	//Establecer semilla
	//srand(seed);
	srand(time(NULL));

	//Grid
	for (int32 i = 0; i < N; i++)
	{
		for (int32 j = 0; j < N; j++)
		{
			grid[i][j] = Vector3(i + (float)rand() / RAND_MAX, j + (float)rand() / RAND_MAX, 0);
		}
	}

	int32 n = 0;
	float dx, dy, dist, minDist, minDist2, maxDist;
	for (int32 i = 0; i < size; i++)
	{
		for (int32 j = 0; j < size; j++)
		{
			minDist  = 99999999.0f;  //"infinito"
			minDist2 = 99999999.0f;  //"infinito"
			maxDist  = 0.0f;

			for (int32 x = 0; x < N; x++)
			{
				for (int32 y = 0; y < N; y++)
				{

					dx = abs(grid[x][y].x - (float)i / pixels);
					dy = abs(grid[x][y].y - (float)j / pixels);

					if (dx > N / 2) dx = N - dx;
					if (dy > N / 2) dy = N - dy;

					dist = sqrt(pow(dx, 2) + pow(dy, 2));

					if (dist < minDist)
					{
						minDist2 = minDist;
						minDist = dist;
					}

					if (dist > maxDist || maxDist == 0.0)
						maxDist = dist;
				}
			}

			matrix[(j * size + i) * 3 + 0] = minDist; //R
			matrix[(j * size + i) * 3 + 1] = minDist; //G
			matrix[(j * size + i) * 3 + 2] = minDist; //B
		}
	}

	return matrix;
}
