#include "../includes/mesh.h"

Mesh::Mesh(const char* filename)
{
    m_filename = filename;
}

Mesh::~Mesh()
{
}

bool Mesh::Init()
{
    return true;
}

void Mesh::End()
{
    m_submeshes.Clear();
}

void Mesh::AddSubmesh( Submesh submesh )
{
    m_submeshes.Add(submesh );
}

void Mesh::RemoveSubmesh( uint32 index )
{
    m_submeshes.RemoveAt( index );
}

const Submesh* Mesh::GetSubmesh( uint32 index ) const
{
    return &m_submeshes[ index ];
}

uint32 Mesh::GetNumberOfSubmeshes()
{
    return m_submeshes.Size();
}