#ifndef DATABASE_H
#define DATABASE_H

#include "Matrix4.h"
#include "Vector3.h"
#include "Quat.h"
#include "Rotaxis.h"
#include "Array.h"
#include "Entity.h"
#include "Camera.h"
#include "PathCamera.h"
#include "Light.h"
#include "Spline.h"


class DataBase
{
public:
    static DataBase* Instance();

    bool Init();
    void End ();

    const float* GetProjectionMatrix();
    const float* GetViewMatrix();
    
    void Update( float elapsed );

    void Render();
    void PrepareLights();

    Camera*        GetCamera();
    Array<Entity*> GetEntities();

private:
    DataBase();
    ~DataBase();

    static DataBase* m_dataBase;

    Camera*        m_camera;
    Array<Entity*> m_entities;
    Light*         m_light;
    Spline*        m_spline;

    uint32 m_vertexShader;
    uint32 m_fragmentShader;
    uint32 m_program;
};


#endif