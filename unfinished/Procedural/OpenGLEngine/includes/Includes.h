//OpenGL
#include "glew.h"
#include "freeglut.h"

//Custom Types
#include "Types.h"
#include "GlobalEnums.h"
#include "DataStructs.h"

//Mathemathics
#include "MathFunctions.h"
#include "Vector3.h"
#include "Rotaxis.h"
#include "Matrix4.h"
#include "Quat.h"
#include "Vertex.h"

//Auxiliar
#include "Array.h"

//Engine
#include "EngineEntity.h"
#include "Renderer.h"
#include "Screen.h"
#include "Texture.h"
#include "Submesh.h"