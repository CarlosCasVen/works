#ifndef CAMERA_H
#define CAMERA_H

#include "Entity.h"
#include "Matrix4.h"
#include "Vector3.h"

class Camera : public Entity {
public:
    Camera();
    ~Camera();

    virtual bool Init();
    virtual void End ();

    int32  GetViewportX     () const;
    int32  GetViewportY     () const;
    uint16 GetViewportWidth () const;
    uint16 GetViewportHeight() const;
    void   SetViewport      ( int32 x, int32 y, uint16 w, uint16 h );

    void SetOrtho      ( float left, float right, float bottom, float top, float n, float f );
    void SetFrustum    ( float left, float right, float bottom, float top, float n, float f );
    void SetPerspective( float fov, float ratio, float n, float f );

    bool UsesTarget() const;
    void SetUsesTarget( bool usesTarget );

    const Vector3& GetTarget() const;
    Vector3& GetTarget();

    const Matrix4& GetProjection() const;
    const Matrix4& GetView();

    virtual void Update( float elapsed );

    void Render();

    virtual uint32 GetVAO();
    virtual uint32 GetEBO(); 
    virtual uint32 GetNumIndices();
    virtual void   ActiveDiffuse();

private:
    Matrix4 m_projMatrix;
    Matrix4 m_viewMatrix;
    int32   m_vx;
    int32   m_vy;
    uint16  m_vw;
    uint16  m_vh;
    bool    m_usesTarget;
    Vector3 m_target;
};

#endif 