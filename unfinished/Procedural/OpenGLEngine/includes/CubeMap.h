#ifndef CUBE_MAP_H
#define CUBE_MAP_H

#include "Entity.h"
#include "Shader.h"

class CubeMap : public Entity
{
public:
    CubeMap();
    ~CubeMap();

    virtual bool Init();
    virtual void End();

    virtual void Render();

private:
    Shader* m_shader;

    uint32  m_vao;
    uint32  m_vbo;
    uint32  m_cubeTexture;
 
};



#endif 