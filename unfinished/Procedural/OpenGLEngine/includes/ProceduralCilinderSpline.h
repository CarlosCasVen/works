#ifndef PROCEDURAL_CILINDER_SPLINE_H
#define PROCEDURAL_CILINDER_SPLINE_H

#include "Entity.h"
#include "Shader.h"
#include "Spline.h"

class ProceduralCilinderSpline : public Entity
{
public:
    ProceduralCilinderSpline( uint16 numHeightDivisions, uint16 numWidthDivisions, float width, float radius, Spline* spline );
    ~ProceduralCilinderSpline();

    virtual bool Init();
    virtual void End();

    virtual void Render();

private:
    Shader* m_shader;

    uint32  m_vao;
    uint32  m_vbo;
    uint32  m_ebo;
    uint16  m_nIndices;
    uint32  m_textureDiffuse;
    Spline* m_spline;

    uint16  m_numHeightDivisions;
    uint16  m_numWidthDivisions;
    float   m_offsetWidth;
    float   m_radius;
};



#endif 