#ifndef LOCATOR_COMPONENT_H
#define LOCATOR_COMPONENT_H

#include "Vector3.h"
#include "Quat.h"
#include "Matrix4.h"

class LocatorComponent
{
public:
    LocatorComponent ();
    ~LocatorComponent();

    virtual bool Init();
    virtual void End ();

    void Translate  ( Vector3 position );
    void Scale      ( Vector3 scale    );
    void Rotate     ( Quat    rotation );
    void Rotate     ( Vector3 rotation );

    Vector3 GetPosition     ();
    Vector3 GetScale        ();
    Quat    GetQuatRotation ();
    Vector3 GetEulerRotation();

    Matrix4 GetMVP();

private:
    Vector3 m_position;
    Vector3 m_scale;
    Vector3 m_eulerRotation;
    Quat    m_quatRotation;

};

#endif