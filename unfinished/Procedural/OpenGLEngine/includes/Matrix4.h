#ifndef MATRIX4_H
#define MATRIX4_H

#include "MathFunctions.h"
#include "Rotaxis.h"
#include "Types.h"
#include "Vector3.h"
#include <string.h>


#define MATRIX_SIZE 16
#define ROW_SIZE     4
#define COLUMN_SIZE  4
#define VECTOR3_SIZE 3

class Matrix4 {
public:
    Matrix4();
    Matrix4( const Matrix4& other );
    Matrix4( const float* values );

    bool operator==( const Matrix4& other ) const;
    bool operator!=( const Matrix4& other ) const;
    Matrix4& operator=( const Matrix4& other );
    Matrix4 operator+( const Matrix4& other ) const;
    Matrix4 operator-( const Matrix4& other ) const;
    Matrix4 operator*( const Matrix4& other ) const;
    Vector3 operator*( const Vector3& vec ) const;
    Matrix4& operator+=( const Matrix4& other );
    Matrix4& operator-=( const Matrix4& other );
    Matrix4& operator*=( const Matrix4& other );
    const float& operator[]( uint32 pos ) const;
    float& operator[]( uint32 pos );

    void SetIdentity();
    void Set( const float* m );
    float& RC( uint32 row, uint32 column );
    const float& RC( uint32 row, uint32 column ) const;
    void SetRC( uint32 row, uint32 column, float value );

    Vector3 Translation() const;

    void SetTranslation( const Vector3& trans );
    void SetRotation( const RotAxis& rot );
    void SetScale( const Vector3& scale );
    void Translate( const Vector3& trans );
    void Rotate( const RotAxis& rot );
    void Scale( const Vector3& scale );

    Matrix4 Transposed() const;
    Matrix4 Inverse() const;

    void SetOrtho( float left, float right, float bottom, float top, float n, float f );
    void SetFrustum( float left, float right, float bottom, float top, float n, float f );
    void SetPerspective( float fovy, float aspect, float n, float f );

    void LookAt( const Vector3& pos, const Vector3& look, const Vector3& up );
private:
    float m[ 16 ];
};

inline Matrix4::Matrix4()
{
    SetIdentity();
}

inline Matrix4::Matrix4( const Matrix4& other )
{
    memcpy( &m, &other.m, sizeof( m ) );
}

inline Matrix4::Matrix4( const float* values )
{
    memcpy( &m, values, sizeof( m ) );
}

inline bool Matrix4::operator==( const Matrix4& other ) const
{
    return memcmp( m, other.m, sizeof( m ) ) == 0;
}

inline bool Matrix4::operator!=( const Matrix4& other ) const
{
    return memcmp( m, other.m, sizeof( m ) ) != 0;
}

inline Matrix4& Matrix4::operator=( const Matrix4& other )
{
    memcpy( m, &other.m, sizeof( m ) );
    return *this;
}

inline Matrix4 Matrix4::operator+( const Matrix4& other ) const
{
    float r[ 16 ];

    for( uint8 i = 0; i < 16; i++ )
    {
        r[ i ] = m[ i ] + other[ i ];
    }

    return Matrix4( r );
}

inline Matrix4 Matrix4::operator-( const Matrix4& other ) const
{
    float r[ 16 ];

    for( uint8 i = 0; i < 16; i++ )
    {
        r[ i ] = m[ i ] - other[ i ];
    }

    return Matrix4( r );
}

inline Matrix4 Matrix4::operator*( const Matrix4& other ) const
{
    float res[ 16 ];
    for( int i = 0; i < 4; ++i ){
        for( int j = 0; j < 4; ++j ){
            res[ i * 4 + j ] = m[ j ] * other.m[ i * 4 ] + m[ j + 4 ] * other.m[ i * 4 + 1 ] + m[ j + 8 ] * other.m[ i * 4 + 2 ] + m[ j + 12 ] * other.m[ i * 4 + 3 ];
        }
    }
    return Matrix4( res );
}

inline Vector3 Matrix4::operator*( const Vector3& vec ) const
{
    Matrix4 m;
    m.SetTranslation( vec );
    Matrix4 m2 = *this * m;

    return m2.Translation();
}

inline Matrix4& Matrix4::operator+=( const Matrix4& other )
{
    for( uint8 i = 0; i < 16; i++ )
    {
        m[ i ] += other[ i ];
    }
}

inline Matrix4& Matrix4::operator-=( const Matrix4& other )
{
    for( uint8 i = 0; i < 16; i++ )
    {
        m[ i ] -= other[ i ];
    }
}

inline Matrix4& Matrix4::operator*=( const Matrix4& other )
{
    *this = *this * other;
    return *this;
}

inline const float& Matrix4::operator[]( uint32 pos ) const
{
    return m[ pos ];
}

inline float& Matrix4::operator[]( uint32 pos )
{
    return m[ pos ];
}

inline void Matrix4::SetIdentity()
{
    memset( &m, 0, sizeof( m ) );
    m[ 0 ] = m[ 5 ] = m[ 10 ] = m[ 15 ] = 1.0f;
}

inline void Matrix4::Set( const float* m )
{
    memcpy( this->m, m, sizeof( this->m ) );
}

inline float& Matrix4::RC( uint32 row, uint32 column )
{
    return m[ column * 4 + row ];
}

inline const float& Matrix4::RC( uint32 row, uint32 column ) const
{
    return m[ column * 4 + row ];
}

inline void Matrix4::SetRC( uint32 row, uint32 column, float value )
{
    m[ column * 4 + row ] = value;
}

inline Vector3 Matrix4::Translation() const
{
    return Vector3( m[ 12 ], m[ 13 ], m[ 14 ] );
}

inline void Matrix4::SetTranslation( const Vector3& trans )
{
    SetIdentity();

    m[ 12 ] = trans.X();
    m[ 13 ] = trans.Y();
    m[ 14 ] = trans.Z();
}

inline void Matrix4::SetRotation( const RotAxis& rot )
{
    float c = static_cast<float>( DegCos( rot.Angle() ) );
    float s = static_cast<float>( DegSin( rot.Angle() ) );
    m[ 0 ] = rot.Axis().X()*rot.Axis().X()*( 1 - c ) + c;
    m[ 1 ] = rot.Axis().X()*rot.Axis().Y()*( 1 - c ) + rot.Axis().Z()*s;
    m[ 2 ] = rot.Axis().X()*rot.Axis().Z()*( 1 - c ) - rot.Axis().Y()*s;
    m[ 4 ] = rot.Axis().X()*rot.Axis().Y()*( 1 - c ) - rot.Axis().Z()*s;
    m[ 5 ] = rot.Axis().Y()*rot.Axis().Y()*( 1 - c ) + c;
    m[ 6 ] = rot.Axis().Y()*rot.Axis().Z()*( 1 - c ) + rot.Axis().X()*s;
    m[ 8 ] = rot.Axis().X()*rot.Axis().Z()*( 1 - c ) + rot.Axis().Y()*s;
    m[ 9 ] = rot.Axis().Y()*rot.Axis().Z()*( 1 - c ) - rot.Axis().X()*s;
    m[ 10 ] = rot.Axis().Z()*rot.Axis().Z()*( 1 - c ) + c;
}

inline void Matrix4::SetScale( const Vector3& scale )
{
    SetIdentity();

    m[ 0 ] = scale.X();
    m[ 5 ] = scale.Y();
    m[ 10 ] = scale.Z();
}

inline void Matrix4::Translate( const Vector3& trans )
{
    Matrix4 op;

    op.SetTranslation( trans );
    *this *= op;
}

inline void Matrix4::Rotate( const RotAxis& rot )
{
    Matrix4 op;

    op.SetRotation( rot );
    *this *= op;
}

inline void Matrix4::Scale( const Vector3& scale )
{
    Matrix4 op;

    op.SetScale( scale );
    *this *= op;
}

inline Matrix4 Matrix4::Transposed() const
{
    float r[ 16 ];

    r[ 0 ] = m[ 0 ];
    r[ 1 ] = m[ 4 ];
    r[ 2 ] = m[ 8 ];
    r[ 3 ] = m[ 12 ];
    r[ 4 ] = m[ 1 ];
    r[ 5 ] = m[ 5 ];
    r[ 6 ] = m[ 9 ];
    r[ 7 ] = m[ 13 ];
    r[ 8 ] = m[ 2 ];
    r[ 9 ] = m[ 6 ];
    r[ 10 ] = m[ 10 ];
    r[ 11 ] = m[ 14 ];
    r[ 12 ] = m[ 3 ];
    r[ 13 ] = m[ 7 ];
    r[ 14 ] = m[ 11 ];
    r[ 15 ] = m[ 15 ];

    return Matrix4( r );
}

inline Matrix4 Matrix4::Inverse() const {
    Matrix4 inv;

    inv[ 0 ] = m[ 5 ] * m[ 10 ] * m[ 15 ] - m[ 5 ] * m[ 11 ] * m[ 14 ] - m[ 9 ] * m[ 6 ] * m[ 15 ] + m[ 9 ] * m[ 7 ] * m[ 14 ] + m[ 13 ] * m[ 6 ] * m[ 11 ] - m[ 13 ] * m[ 7 ] * m[ 10 ];
    inv[ 4 ] = -m[ 4 ] * m[ 10 ] * m[ 15 ] + m[ 4 ] * m[ 11 ] * m[ 14 ] + m[ 8 ] * m[ 6 ] * m[ 15 ] - m[ 8 ] * m[ 7 ] * m[ 14 ] - m[ 12 ] * m[ 6 ] * m[ 11 ] + m[ 12 ] * m[ 7 ] * m[ 10 ];
    inv[ 8 ] = m[ 4 ] * m[ 9 ] * m[ 15 ] - m[ 4 ] * m[ 11 ] * m[ 13 ] - m[ 8 ] * m[ 5 ] * m[ 15 ] + m[ 8 ] * m[ 7 ] * m[ 13 ] + m[ 12 ] * m[ 5 ] * m[ 11 ] - m[ 12 ] * m[ 7 ] * m[ 9 ];
    inv[ 12 ] = -m[ 4 ] * m[ 9 ] * m[ 14 ] + m[ 4 ] * m[ 10 ] * m[ 13 ] + m[ 8 ] * m[ 5 ] * m[ 14 ] - m[ 8 ] * m[ 6 ] * m[ 13 ] - m[ 12 ] * m[ 5 ] * m[ 10 ] + m[ 12 ] * m[ 6 ] * m[ 9 ];
    inv[ 1 ] = -m[ 1 ] * m[ 10 ] * m[ 15 ] + m[ 1 ] * m[ 11 ] * m[ 14 ] + m[ 9 ] * m[ 2 ] * m[ 15 ] - m[ 9 ] * m[ 3 ] * m[ 14 ] - m[ 13 ] * m[ 2 ] * m[ 11 ] + m[ 13 ] * m[ 3 ] * m[ 10 ];
    inv[ 5 ] = m[ 0 ] * m[ 10 ] * m[ 15 ] - m[ 0 ] * m[ 11 ] * m[ 14 ] - m[ 8 ] * m[ 2 ] * m[ 15 ] + m[ 8 ] * m[ 3 ] * m[ 14 ] + m[ 12 ] * m[ 2 ] * m[ 11 ] - m[ 12 ] * m[ 3 ] * m[ 10 ];
    inv[ 9 ] = -m[ 0 ] * m[ 9 ] * m[ 15 ] + m[ 0 ] * m[ 11 ] * m[ 13 ] + m[ 8 ] * m[ 1 ] * m[ 15 ] - m[ 8 ] * m[ 3 ] * m[ 13 ] - m[ 12 ] * m[ 1 ] * m[ 11 ] + m[ 12 ] * m[ 3 ] * m[ 9 ];
    inv[ 13 ] = m[ 0 ] * m[ 9 ] * m[ 14 ] - m[ 0 ] * m[ 10 ] * m[ 13 ] - m[ 8 ] * m[ 1 ] * m[ 14 ] + m[ 8 ] * m[ 2 ] * m[ 13 ] + m[ 12 ] * m[ 1 ] * m[ 10 ] - m[ 12 ] * m[ 2 ] * m[ 9 ];
    inv[ 2 ] = m[ 1 ] * m[ 6 ] * m[ 15 ] - m[ 1 ] * m[ 7 ] * m[ 14 ] - m[ 5 ] * m[ 2 ] * m[ 15 ] + m[ 5 ] * m[ 3 ] * m[ 14 ] + m[ 13 ] * m[ 2 ] * m[ 7 ] - m[ 13 ] * m[ 3 ] * m[ 6 ];
    inv[ 6 ] = -m[ 0 ] * m[ 6 ] * m[ 15 ] + m[ 0 ] * m[ 7 ] * m[ 14 ] + m[ 4 ] * m[ 2 ] * m[ 15 ] - m[ 4 ] * m[ 3 ] * m[ 14 ] - m[ 12 ] * m[ 2 ] * m[ 7 ] + m[ 12 ] * m[ 3 ] * m[ 6 ];
    inv[ 10 ] = m[ 0 ] * m[ 5 ] * m[ 15 ] - m[ 0 ] * m[ 7 ] * m[ 13 ] - m[ 4 ] * m[ 1 ] * m[ 15 ] + m[ 4 ] * m[ 3 ] * m[ 13 ] + m[ 12 ] * m[ 1 ] * m[ 7 ] - m[ 12 ] * m[ 3 ] * m[ 5 ];
    inv[ 14 ] = -m[ 0 ] * m[ 5 ] * m[ 14 ] + m[ 0 ] * m[ 6 ] * m[ 13 ] + m[ 4 ] * m[ 1 ] * m[ 14 ] - m[ 4 ] * m[ 2 ] * m[ 13 ] - m[ 12 ] * m[ 1 ] * m[ 6 ] + m[ 12 ] * m[ 2 ] * m[ 5 ];
    inv[ 3 ] = -m[ 1 ] * m[ 6 ] * m[ 11 ] + m[ 1 ] * m[ 7 ] * m[ 10 ] + m[ 5 ] * m[ 2 ] * m[ 11 ] - m[ 5 ] * m[ 3 ] * m[ 10 ] - m[ 9 ] * m[ 2 ] * m[ 7 ] + m[ 9 ] * m[ 3 ] * m[ 6 ];
    inv[ 7 ] = m[ 0 ] * m[ 6 ] * m[ 11 ] - m[ 0 ] * m[ 7 ] * m[ 10 ] - m[ 4 ] * m[ 2 ] * m[ 11 ] + m[ 4 ] * m[ 3 ] * m[ 10 ] + m[ 8 ] * m[ 2 ] * m[ 7 ] - m[ 8 ] * m[ 3 ] * m[ 6 ];
    inv[ 11 ] = -m[ 0 ] * m[ 5 ] * m[ 11 ] + m[ 0 ] * m[ 7 ] * m[ 9 ] + m[ 4 ] * m[ 1 ] * m[ 11 ] - m[ 4 ] * m[ 3 ] * m[ 9 ] - m[ 8 ] * m[ 1 ] * m[ 7 ] + m[ 8 ] * m[ 3 ] * m[ 5 ];
    inv[ 15 ] = m[ 0 ] * m[ 5 ] * m[ 10 ] - m[ 0 ] * m[ 6 ] * m[ 9 ] - m[ 4 ] * m[ 1 ] * m[ 10 ] + m[ 4 ] * m[ 2 ] * m[ 9 ] + m[ 8 ] * m[ 1 ] * m[ 6 ] - m[ 8 ] * m[ 2 ] * m[ 5 ];

    float det = m[ 0 ] * inv[ 0 ] + m[ 1 ] * inv[ 4 ] + m[ 2 ] * inv[ 8 ] + m[ 3 ] * inv[ 12 ];
    if( fabs( det ) <= 0.00001f )
        return Matrix4();

    float invdet = 1.0f / det;
    for( uint32 i = 0; i < 16; i++ )
        inv[ i ] *= invdet;

    return inv;
}

inline void Matrix4::SetOrtho( float left, float right, float bottom, float top, float n, float f )
{
    float rl = 1 / ( right - left );
    float tb = 1 / ( top - bottom );
    float fn = 1 / ( f - n );

    this->SetIdentity();
    m[ 0 ] = 2 * rl;
    m[ 5 ] = 2 * tb;
    m[ 10 ] = -2 * fn;
    m[ 12 ] = -( right + left ) * rl;
    m[ 13 ] = -( top + bottom ) * tb;
    m[ 14 ] = -( f + n ) * fn;
}

inline void Matrix4::SetFrustum( float left, float right, float bottom, float top, float n, float f )
{
    float rl = 1 / ( right - left );
    float tb = 1 / ( top - bottom );
    float fn = 1 / ( f - n );

    this->SetIdentity();
    m[ 0 ] = 2 * n * rl;
    m[ 5 ] = 2 * n * tb;
    m[ 8 ] = ( right + left ) * rl;
    m[ 9 ] = ( top + bottom ) * tb;
    m[ 10 ] = -( f + n ) * fn;
    m[ 11 ] = -1;
    m[ 14 ] = -2 * f * n * fn;
    m[ 15 ] = 0;
}

inline void Matrix4::SetPerspective( float fovy, float aspect, float n, float f )
{
    float height = n * ( float ) DegTan( fovy / 2 );
    float width = height * aspect;
    SetFrustum( -width, width, -height, height, n, f );
}

inline void Matrix4::LookAt( const Vector3& pos, const Vector3& look, const Vector3& up )
{
    Vector3 z = pos - look;
    z = z.Normalized();
    Vector3 x = up;
    x = x.Cross( z );
    Vector3 y = z;
    y = y.Cross( x );
    x = x.Normalized();
    y = y.Normalized();

    m[ 0 ] = x.X();	m[ 4 ] = x.Y();	m[ 8 ] = x.Z();	m[ 12 ] = 0;
    m[ 1 ] = y.X();	m[ 5 ] = y.Y();	m[ 9 ] = y.Z();	m[ 13 ] = 0;
    m[ 2 ] = z.X();	m[ 6 ] = z.Y();	m[ 10 ] = z.Z();	m[ 14 ] = 0;
    m[ 3 ] = 0;		m[ 7 ] = 0;		m[ 11 ] = 0;		m[ 15 ] = 1;

    this->Translate( Vector3( -pos.X(), -pos.Y(), -pos.Z() ) );
}

#endif