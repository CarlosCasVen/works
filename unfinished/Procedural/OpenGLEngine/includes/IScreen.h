#ifndef ISCREEN_H
#define ISCREEN_H

#include "Types.h"

class IScreen 
{
public:
    static IScreen& Instance();

    virtual bool Init() = 0;
    virtual void End () = 0;
    
    //Set options
    virtual void SetFullscreen( bool fullscreen             ) = 0;
    virtual void SetWindowName( const char* name            ) = 0;
    virtual void SetWindowSize( int16 width, int16 height   ) = 0;
     
    //Get Options
    virtual float GetElapsedTime(                              ) const = 0;
    virtual void  GetWindowSize( uint16* width, uint16* height ) const = 0;

    virtual void Update( float elapsed ) = 0;
    
    virtual void Clear( uint8 r, uint8 g, uint8 b, uint8 a = 255 ) = 0;
};

#endif