#ifndef IMAGELOADER_H
#define IMAGELOADER_H

#include "../includes/CString.h"
#include "../includes/Vector3.h"
#include <vector>

class ImageLoader {
public:
	static bool Load(CString path, uint8 &image_buffer, int32 &width, int32 &height);
	static bool IsPOT(int32 x);
};

#endif