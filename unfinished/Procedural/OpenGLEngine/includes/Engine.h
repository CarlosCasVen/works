#ifndef ENGINE_H
#define ENGINE_H

namespace Engine
{
    void Init       ();
    void ReshapeFunc( int width, int height );
    void MainLoop   ();
    void Display    ();
    void Input      ( unsigned char key, int x, int y );
    void End        ();
};

#endif