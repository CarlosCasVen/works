#ifndef PATH_CAMERA_H
#define PATH_CAMERA_H

#include "Camera.h"
#include "Matrix4.h"
#include "Vector3.h"
#include "Spline.h"

class PathCamera : public Camera 
{
public:
    PathCamera( Spline * spline );
    ~PathCamera();

    virtual void Update( float elapsed );

    void SetTime( uint32 t = 0 );
    void Pause();
    void Resume();
    void SetLoop( bool looping = false );

private:
	Array<Vector3> m_path;
    uint32 m_t;
    bool  m_pause;
    bool  m_isLooping;
};

#endif 