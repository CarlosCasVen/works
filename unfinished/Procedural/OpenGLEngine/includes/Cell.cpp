#include "../includes/Cell.h"
#include "../includes/Renderer.h"
#include "../includes/Vertex.h"
#include "../includes/MathFunctions.h"
#include "../includes/PerlinNoise.h"
#include "../includes/DataBase.h"
#include "../includes/Renderer.h"
#include "../includes/ModelLoader.h"
#include <stdio.h>
#include <stddef.h>
#include <time.h>

#define VERTEX_SHADER_PATH "data/vCellShader.txt"
#define FRAGMENT_SHADER_PATH "data/fCellShader.txt"

Cell::Cell( const char* model, Vector3 color ) : Entity()
{
    m_path  = model;
    m_color = color;

    m_shader = new Shader( VERTEX_SHADER_PATH, FRAGMENT_SHADER_PATH );
}

Cell::~Cell()
{
    delete m_shader;
}

bool Cell::Init()
{
    Array<Vertex> dataMesh;
    Array<unsigned int> indexes;

    std::vector<uint32> indices;
    std::vector<Vector3> vertices;
    std::vector<Vector3> uvs;
    std::vector<Vector3> normals;

    ModelLoader::Load( m_path, indices, vertices, uvs, normals );
    
    for( unsigned int index = 0; index < vertices.size(); index++ )
    {
        dataMesh.Add(
                        Vertex(
                               vertices[ index ],
                               uvs[index].X(),
                               uvs[index].Y(),
                               normals[index]
                        )
                    );

    }

    for( unsigned int index = 0; index < indices.size(); index++ )
    {
        indexes.Add(indices[index]);
    } 

    Renderer& render = Renderer::Instance();

    //GENERACION DE MALLA 

    m_vao = render.CreateVAO();
    m_vbo = render.CreateVBO();
    m_ebo = render.CreateVBO();

    render.BindVAO( m_vao );
    render.BindVBO( m_vbo, GL_ARRAY_BUFFER );

    glBufferData( GL_ARRAY_BUFFER, sizeof( Vertex ) * dataMesh.Size(), &dataMesh[ 0 ], GL_STATIC_DRAW );

    //enlace de los vertices
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, sizeof( Vertex ), ( const void* ) offsetof( Vertex, m_position ) );
    glEnableVertexAttribArray( 0 );

    //enlace de las normales
    glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, sizeof( Vertex ), ( const void* ) offsetof( Vertex, m_normal ) );
    glEnableVertexAttribArray( 1 );

    //enlace de las texcoords
    glVertexAttribPointer( 2, 2, GL_FLOAT, GL_FALSE, sizeof( Vertex ), ( const void* ) offsetof( Vertex, m_u ) );
    glEnableVertexAttribArray( 2 );

    render.BindVBO( m_ebo, GL_ELEMENT_ARRAY_BUFFER );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( unsigned int ) * indexes.Size(), &indexes[ 0 ], GL_STATIC_DRAW );

    render.BindVBO( 0, GL_ELEMENT_ARRAY_BUFFER );
    render.BindVBO( 0, GL_ARRAY_BUFFER );
    render.BindVAO( 0 );

    m_nIndexes = ( uint16 ) indexes.Size();

    return true;
}

void Cell::End()
{
    Renderer::Instance().FreeVAOBuffers( m_vao );
    Renderer::Instance().FreeVBOBuffers( m_vbo );
    Renderer::Instance().FreeVBOBuffers( m_ebo );
}

void Cell::Render()
{
    m_shader->ActiveProgram();

    Matrix4 projection = DataBase::Instance()->GetCamera()->GetProjection();
    Matrix4 view = DataBase::Instance()->GetCamera()->GetView();

    Matrix4 model = GetMoldeMatrix();
    Matrix4 mvp = projection * view * model;

    Renderer::Instance().SetUniformMatrix( 0, model );
    Renderer::Instance().SetUniformMatrix( 1, mvp );
    Renderer::Instance().SetUniformInt( 8, 16 );
    Renderer::Instance().SetUniformVec3( 11, m_color );

    Renderer::Instance().BindVAO( m_vao );
    Renderer::Instance().BindVBO( m_ebo, GL_ELEMENT_ARRAY_BUFFER );

    glDrawElements( m_typeOfDraw, m_nIndexes, GL_UNSIGNED_INT, 0 );

    Renderer::Instance().BindVAO( 0 );
    Renderer::Instance().BindVBO( 0, GL_ELEMENT_ARRAY_BUFFER );

    Renderer::Instance().BindTexture2D( 0 );


    m_shader->DescActiveProgram();
}