#ifndef VECTOR_ENTITY_H
#define VECTOR_ENTITY_H

#include "Entity.h"
#include "Shader.h"
#include "Spline.h"

class VectorEntity : public Entity
{
public:
    VectorEntity( Vector3 origin, Vector3 dir, Vector3 color, float distance = 100.0f );
    ~VectorEntity();

    virtual bool Init();
    virtual void End();

    virtual void Render();

private:
    Shader* m_shader;

    uint32  m_vao;
    uint32  m_vbo;

    float   m_distance;
    Vector3 m_color;
    Vector3 m_dir;
    Vector3 m_origin;
};



#endif 