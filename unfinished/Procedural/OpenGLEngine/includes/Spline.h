#ifndef SPLINE_H
#define SPLINE_H

#include "Vector3.h"
#include "Array.h"

class Spline
{
public:
    Spline( float r = .5f, float dt = 0 );
    ~Spline();

    void SetDeltaTime( float dt );
    void SetBlending( float r );

    void AddPoint( Vector3 point );
    Vector3 GetPoint( int point );
    Array<Vector3> GetPoints();

	Vector3 Get(float t);
	Array<Vector3> GenerateSamples(uint32 samples);

private:
	Vector3 Interpolate(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3);
    uint32 SafeLimit( int32 point );

    float m_dt;
    float m_r;
    Array<Vector3> m_points;
};

#endif
