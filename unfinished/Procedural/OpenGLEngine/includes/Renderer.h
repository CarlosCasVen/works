#ifndef RENDERER_H
#define RENDERER_H

#include "CString.h"
#include "Matrix4.h"
#include "Locations.h"
#include "Types.h"
#include "Shader.h"

#define NUMBER_TEXTURES_GEOMETRY_FBO 4
#define NUMBER_TEXTURES_LIGHT_FBO 1


class Renderer
{
public:
    static Renderer& Instance();

    virtual bool Init();
    virtual void End ();

    virtual void ConfigRender();

    virtual uint32 CreateVBO     (                     );
    virtual uint32 CreateVAO     (                     );
    virtual void   BindVBO       ( uint32 id, int type );
    virtual void   BindVAO       ( uint32 id           );
    virtual void   FreeVBOBuffers( uint32 id           );
    virtual void   FreeVAOBuffers( uint32 id           );

    virtual uint32 CreateTexture  (                                                  );
    virtual void   BindTexture2D  ( uint32 texture                                   );
    virtual void   ConfigTexture2D(                                                  );
    virtual void   ActivateTexture( uint32 textureUnit, uint32 texture, int location );
    virtual void   FreeTexture    ( uint32 texture               );

    virtual void ConfigureCubeMap();
    virtual void DataFaceCubeMap ( uint32 face, uint8* data, uint32 width, uint32 height );
    
    virtual void SetUniformMatrix( uint32 location, Matrix4 matrix   );
    virtual void SetUniformInt   ( uint32 location, int value        );
    virtual void SetUniformFloat ( uint32 location, float value      );
    virtual void SetUniformVec3  ( uint32 location, Vector3 position );
    virtual void SetUniformVec4  ( uint32 location, float* position  );

    virtual void DeferredShading();

    virtual void SwapBuffers();

private:
    Renderer ();
    ~Renderer();

    bool InitGeometryFBO( uint16 width, uint16 height );
    bool InitLightFBO   ( uint16 width, uint16 height );

    void LinkFrameBuffers();
    void ResizeTexturesFBOs( uint16 width, uint16 height );

    void GeometryRender();
    void LightRender   ();
    void PostProcessing();

    void CreateQuad();

    uint32 m_texturesGeometryFbo[ NUMBER_TEXTURES_GEOMETRY_FBO ];
    uint32 m_texturesLightFbo   [ NUMBER_TEXTURES_LIGHT_FBO    ];

    uint32 m_gFbo;
    uint32 m_lFbo;

    uint32  m_vaoQuad;
    uint32  m_vboQuad;
    uint32  m_eboQuad;
    Shader* m_lightShader;

    Shader* m_postProccessingShader;
};


#endif