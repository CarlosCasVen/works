#ifndef MODELLOADER_H
#define MODELLOADER_H

#include "../includes/CString.h"
#include "../includes/Vector3.h"
#include <vector>

class ModelLoader {
public:
	static bool Load(
		CString path,
		std::vector<uint32> & indices,
		std::vector<Vector3> & vertices,
		std::vector<Vector3> & uvs,
		std::vector<Vector3> & normals
		);
};

#endif