#ifndef VORONOI_H
#define VORONOI_H

#include "Types.h"

class Voronoi
{
public:
	static float * Generate(int32 seed, int32 size, int32 N);
};

#endif