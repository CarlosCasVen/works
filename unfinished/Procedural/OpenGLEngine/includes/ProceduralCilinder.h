#ifndef PROCEDURAL_CILINDER_H
#define PROCEDURAL_CILINDER_H

#include "Entity.h"
#include "Shader.h"

class ProceduralCilinder : public Entity
{
public:
    ProceduralCilinder( uint16 numHeightDivisions, uint16 numWidthDivisions, float width, float height, float radius );
    ~ProceduralCilinder();

    virtual bool Init();
    virtual void End();

    virtual void Render();

private:
    Shader* m_shader;

    uint32  m_vao;
    uint32  m_vbo;
    uint32  m_ebo;
    uint16  m_nIndices;
    uint32  m_textureDiffuse;

    uint16  m_numHeightDivisions;
    uint16  m_numWidthDivisions;
    float   m_offsetWidth;
    float   m_offsetHeight;
    float   m_radius;
};



#endif 