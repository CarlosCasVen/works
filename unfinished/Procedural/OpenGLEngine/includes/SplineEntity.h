#ifndef SPLINE_ENTITY_H
#define SPLINE_ENTITY_H

#include "Entity.h"
#include "Spline.h"
#include "Shader.h"

class SplineEntity : public Entity
{
public:
    SplineEntity( Spline* spline );
    ~SplineEntity();

    virtual bool Init();
    virtual void End();

    virtual uint32 GetVAO();
    virtual uint32 GetEBO();
    virtual uint32 GetNumIndices();
    virtual void   ActiveDiffuse();

	virtual void Render();

private:
    uint32  m_vao;
    uint32  m_vbo;
    uint32  m_ebo;
    uint16  m_nIndices;
    uint32  m_textureDiffuse;

    Spline* m_spline;
	Shader* m_shader;
};



#endif 