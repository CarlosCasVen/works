#ifndef GLOBAL_ENUMS_H
#define GLOBAL_ENUMS_H

enum TTexture
{
    NORMAL_MAP,
    DIFFUSE_MAP,
    LIGHT_MAP,
    BUMPED_MAP,
    SPECULAR_MAP,
    HEIGHT_MAP,
    GLOSS_MAP,
    OTHER_MAP
};

enum TLight
{
    DIRECTIONAL_LIGHT,
    POINT_LIGHT,
    SPOT_LIGHT
};

enum TEngineComponent
{
    ENGINE_COMPONENT,
    USER_DEFINED,
    NON_DEFINED
};

enum TEngineEntity
{
    ENGINE_ENTITY,
    LIGHT_ENTITY,
    MESH_ENTITY,
    PARTICLE_ENTITY,
    CAMERA_ENTITY,
    UNDEFINED
};

#endif