#ifndef UGINE_VERTEX_H
#define UGINE_VERTEX_H

#include "vector3.h"

struct Vertex {
	Vertex();
	Vertex(const Vector3& pos, float u, float v, const Vector3& normal );
	bool operator==(const Vertex& other) const;

	Vector3 m_position;
	float   m_u;
	float   m_v;
	Vector3 m_normal;
};

inline Vertex::Vertex() {
	m_position = Vector3(0,0,0);
	m_u = 0;
	m_v = 0;
	m_normal = Vector3( 0, 0, 0 );
}

inline Vertex::Vertex(const Vector3& pos, float u, float v, const Vector3& normal) {
	m_position	= pos;
	m_u			= u;
	m_v			= v;
	m_normal	= normal;
}

inline bool Vertex::operator==(const Vertex& other) const {
    return m_position == other.m_position && m_u == other.m_u && m_v == other.m_v && m_normal == other.m_normal;
}

#endif