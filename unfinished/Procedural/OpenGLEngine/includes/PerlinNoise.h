#include <vector>

#ifndef PERLINNOISE_H
#define PERLINNOISE_H

#include "Types.h"

class PerlinNoise
{
public:
	PerlinNoise(uint32 seed);

	float get(float x, float y, float z);
private:
	float lerp(float t, float a, float b);
	float grad(int hash, float x, float y, float z);

	std::vector<int32> m_permutation;
};

#endif