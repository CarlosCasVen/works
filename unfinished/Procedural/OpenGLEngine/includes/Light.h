#ifndef UGINE_LIGHT_H
#define UGINE_LIGHT_H

#include "Entity.h"
#include "Renderer.h"
#include "Vector3.h"

enum TLight {
    DIRECTIONAL_LIGHT,
    POINT_LIGHT,
    SPOT_LIGHT,
    NONE
};

class Light : public Entity
{

public:
    Light ();
    ~Light();
    
    virtual bool Init();
    virtual void End();

    virtual uint32 GetVAO();
    virtual uint32 GetEBO();
    virtual uint32 GetNumIndices();
    virtual void   ActiveDiffuse();
    
    TLight GetType      () const;
   	float  GetRed        () const;
    float  GetGreen      () const;
    float  GetBlue       () const;
    float  GetAttenuation() const;

    void SetColor       ( float red, float green, float blue );
	void SetAttenuation ( float att                          );
    void SetType        ( TLight type                        );

    void SetEnable( bool enable );
    bool IsEnable();

private:
	TLight  m_type;
	Vector3 m_color;
	float   m_attenuation;
    float   m_spotAngle;
    bool    m_isEnable;

};



#endif