#ifndef SHADER_H
#define SHADER_H

#include "Types.h"
#include "CString.h"

class Shader
{
public:
    Shader( const char* vertexShaderPath, const char* fragmentShaderPath, const char* geometryShaderPath = NULL, const char* tesselationShaderPath = NULL );
    ~Shader();

    void   ActiveProgram    ();
    void   DescActiveProgram();

private:
    uint32 CreateShader( const CString& source, int type );
    
    uint16 m_vShader;
    uint16 m_fShader;
    uint16 m_gShader;
    uint16 m_tShader;
    uint16 m_program;
};


#endif 