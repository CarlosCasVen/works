#ifndef TEXTURE_H
#define TEXTURE_H

class Texture
{
public:
    Texture ();
    ~Texture();

    virtual bool Init();
    virtual void End ();
};


#endif