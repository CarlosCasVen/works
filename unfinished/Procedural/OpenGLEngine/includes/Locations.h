#ifndef LOCATIONS_H
#define LOCATIONS_H

#define VERTICES_LOC  0
#define NORMS_LOC     1
#define UV_COORDS_LOC 2
#define FRAGMENTS_LOC 3
#define DIFFUSE_LOC   4
#define SPECULAR_LOC  5
#define NORMAL_LOC    6
#define GLOSS_LOC     7
#define AMB_LOC       8
#define DEPTH_BUFFER  9


#endif