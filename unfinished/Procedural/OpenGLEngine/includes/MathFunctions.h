#ifndef MATHFUNCTIONS_H
#define MATHFUNCTIONS_H

#include <math.h>
#include <stdlib.h>
#include "Vector3.h"

#define DEG2RAD 0.0174532925
#define RAD2DEG 57.2957795

enum TPlane
{
    XY,
    YZ,
    XZ
};

template <typename T> T Min(T a, T b) {
	return (a < b) ? a : b;
}

template <typename T> T Max(T a, T b) {
	return (a > b) ? a : b;
}

inline double Log2(double x) {
	return log(x) / log(2.0);
}

inline double Log10(double x) {
	return log(x) / log(10.0);
}

inline double DegSin(double degrees) {
	return sin(DEG2RAD * degrees);
}

inline double DegCos(double degrees) {
	return cos(DEG2RAD * degrees);
}

inline double DegTan(double degrees) {
	return tan(DEG2RAD * degrees);
}

inline double DegASin(double sin) {
	return asin(sin) * RAD2DEG;
}

inline double DegACos(double cos) {
	return acos(cos) * RAD2DEG;
}

inline double DegATan(double tan) {
	return atan(tan) * RAD2DEG;
}

inline double DegATan2(double y, double x) {
	return atan2(y, x) * RAD2DEG;
}

inline int Rand(int min, int max) {
	return min + (rand() % (max - min));
}

inline double Rand(double min, double max) {
	return min + (rand() % (int)(max*1000 - min*1000))/1000.0f;
}

inline double Wrap(double val, double mod) {
    if (mod == 0) return val;
    return val - mod*floor(val/mod);
}

inline double Interpolate(double id, double prevId, double nextId, double prevVal, double nextVal) {
	return prevVal + (nextVal - prevVal) * ((id - prevId) / (nextId - prevId));
}

inline float AngleBetweenVectors( Vector3 a, Vector3 b, TPlane plane )
{
    Vector3 aOnPlane;
    Vector3 bOnPlane;
    float   dir = 1.0f;

    switch( plane )
    {
    case XY:
    {
        aOnPlane = Vector3( a.X(), a.Y(), 0.0f );
        bOnPlane = Vector3( b.X(), b.Y(), 0.0f );

        Vector3 crossProduct = aOnPlane.Cross( bOnPlane );

        dir = crossProduct.Z();
        break;
    }
    case YZ:
    {
        aOnPlane = Vector3( 0.0f, a.Y(), a.Z() );
        bOnPlane = Vector3( 0.0f, b.Y(), b.Z() );

        Vector3 crossProduct = aOnPlane.Cross( bOnPlane );

        dir = crossProduct.X();
        break;
    }
    case XZ:
    {
        aOnPlane = Vector3( a.X(), 0.0f, a.Z() );
        bOnPlane = Vector3( b.X(), 0.0f, b.Z() );

        Vector3 crossProduct = aOnPlane.Cross( bOnPlane );

        dir = crossProduct.Y();
        break;
    }
    }

    float dotProduct     = aOnPlane.Dot( bOnPlane );
    float modAB          = aOnPlane.Length() * bOnPlane.Length();
    

    return dir * static_cast<float>( DegACos( static_cast<float>( dotProduct / modAB ) ) );
}

#undef DEG2RAD
#undef RAD2DEG

#endif 
