#ifndef CSTRING_H
#define CSTRING_H

#pragma warning (disable : 4996)

#include "array.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WIN32
#define realpath(N,R) _fullpath((R),(N),_MAX_PATH)
#endif

class CString {
public:
    CString(const char* str = "");
    CString( const CString& str );
    CString(unsigned int length, char c);
	CString(char c);
    ~CString();

    bool operator==(const CString& other) const;
    bool operator!=(const CString& other) const;
    bool operator<(const CString& other) const;
    bool operator>(const CString& other) const;
    CString operator+( const CString& other ) const;
    CString operator+(char c) const;
    CString& operator=( const CString& other );
    CString& operator=(char c);
    CString& operator+=( const CString& other );
    CString& operator+=(char c);
    char& operator[](unsigned int pos);
    const char& operator[](unsigned int pos) const;

    unsigned int Length() const { return length; }

    static CString FromInt(int val);
    static CString HexFromInt(int val);
    static CString FromFloat(float val);

    int ToInt() const;
    float ToFloat() const;
    const char* ToCString() const { return buffer; }

    static CString Chr( unsigned char val ) { unsigned char c[] = { val, 0 }; return CString( ( char* ) c ); }
    unsigned char Asc() const { return buffer[0]; }

    CString Left(int n) const;
    CString Right(int n) const;
    CString Mid(int ofs, int n);
    CString Replace( const CString& find, const CString& rep ) const;
    int Find( const CString& str, int ofs ) const;

    CString Upper() const;
    CString Lower() const;
    CString LTrim() const;
    CString RTrim() const;
    CString Trim() const;
    CString LSet(int len, const CString& c) const;
    CString RSet(int len, const CString& c) const;

    CString StripExt() const;
    CString StripDir() const;
    CString ExtractExt() const;
    CString ExtractDir() const;
    CString RealPath() const;

    static CString Read( const CString& filename );
    void Write( const CString& filename, bool append = true ) const;

    Array<CString> Split( const CString& delim ) const;
private:
    char* buffer;
    unsigned int length;
    unsigned int bufferSize;
};

inline CString::CString( const char* str ) {
    length = strlen(str);
    bufferSize = sizeof(char)*(length+1);
    buffer = (char*)malloc(bufferSize);
    strcpy(buffer, str);
}

inline CString::CString( const CString& str ) {
    length = str.length;
    bufferSize = sizeof(char)*(length+1);
    buffer = (char*)malloc(bufferSize);
    strcpy(buffer, str.buffer);
}

inline CString::CString(unsigned int length, char c) {
    this->length = length;
    bufferSize = sizeof(char)*(length+1);
    buffer = (char*)malloc(bufferSize);
    memset(buffer, (int)c, length);
    buffer[length] = '\0';
}

inline CString::CString(char c) {
	this->length = 1;
	bufferSize = sizeof(char)*(length+1);
	buffer = (char*)malloc(bufferSize);
	buffer[0] = c;
	buffer[1] = '\0';
}

inline CString::~CString() {
    free(buffer);
}

inline bool CString::operator==(const CString& other) const {
    return strcmp(buffer, other.buffer) == 0;
}

inline bool CString::operator!=(const CString& other) const {
    return strcmp(buffer, other.buffer) != 0;
}

inline bool CString::operator<(const CString& other) const {
    return strcmp(buffer, other.buffer) < 0;
}

inline bool CString::operator>(const CString& other) const {
    return strcmp(buffer, other.buffer) > 0;
}

inline CString CString::operator+(const CString& other) const {
    char* buf = (char*)malloc(length + other.length + 1);
    strcpy(buf, buffer);
    strcat(buf, other.buffer);
    CString str(buf);
    free(buf);
    return str;
}

inline CString CString::operator+(char c) const {
    char s[2] = {c, '\0'};
    return *this + s;
}

inline CString& CString::operator=(const CString& other) {
    if ( bufferSize < other.length + 1 ) {
        bufferSize = sizeof(char)*(other.length+1);
        free(buffer);
        buffer = (char*)malloc(bufferSize);
    }
    length = other.length;
    strcpy(buffer, other.buffer);
    return *this;
}

inline CString& CString::operator=(char c) {
    char s[2] = {c, '\0'};
    return *this = s;
}

inline CString& CString::operator+=(const CString& other) {
    return *this = *this + other;
}

inline CString& CString::operator+=(char c) {
    char s[2] = {c, '\0'};
    return *this += s;
}

inline char& CString::operator[](unsigned int pos) {
    return buffer[pos];
}

inline const char& CString::operator[](unsigned int pos) const {
    return buffer[pos];
}

inline CString CString::FromInt(int val) {
    char buf[32];
    sprintf(buf, "%i", val);
    return CString(buf);
}

inline CString CString::HexFromInt(int val) {
    char buf[32];
    sprintf(buf, "%x", val);
    return CString(buf);
}

inline CString CString::FromFloat(float val) {
    char buf[32];
	if ( val != val ) val = 0;	// NaN
    sprintf(buf, "%f", val);
    return CString(buf);
}

inline int CString::ToInt() const {
    int ret = 0;
    sscanf(buffer, "%i", &ret);
    return ret;
}

inline float CString::ToFloat() const {
    float ret = 0;
    sscanf(buffer, "%f", &ret);
    return ret;
}

inline CString CString::Left(int n) const {
    char* buf = (char*)malloc(n+1);
    strncpy(buf, buffer, n);
    buf[n] = '\0';
    CString str(buf);
    free(buf);
    return str;
}

inline CString CString::Right(int n) const {
    char* buf = (char*)malloc(n+1);
    strncpy(buf, &buffer[length-n], n);
    buf[n] = '\0';
    CString str(buf);
    free(buf);
    return str;
}

inline CString CString::Mid(int ofs, int n) {
    char* buf = (char*)malloc(n+1);
    strncpy(buf, &buffer[ofs], n);
    buf[n] = '\0';
    CString str(buf);
    free(buf);
    return str;
}

inline CString CString::Replace(const CString& find, const CString& rep) const {
    CString str;

    // If string is not found, return unmodified string
    if ( !strstr(buffer, find.buffer) ) return *this;

    // Replace all ocurrences
    const char* p = NULL;
    const char* lastp = buffer;
    while ( (p = strstr(lastp, find.buffer)) ) {
        // Copy chars until p
        while ( lastp != p ) {
            str += *lastp;
            lastp++;
        }

        // Copy replacement chars
        for ( unsigned int i = 0; i < rep.length; i++ ) {
            str += rep.buffer[i];
        }

        // Move lastp to after the replaced string
        lastp += find.length;
    }

    // Copy remaining chars
    while ( *lastp != '\0' ) {
        str += *lastp;
        lastp++;
    }

    return str;
}

inline int CString::Find(const CString& str, int ofs) const {
    const char* p = strstr(&buffer[ofs], str.buffer);
    if ( p == NULL )
        return -1;
    else
        return (p - buffer);
}

inline CString CString::Upper() const {
    CString str;
    for ( unsigned int i = 0; i < length; i++ )
        str += (char)toupper(buffer[i]);
    return str;
}

inline CString CString::Lower() const {
    CString str;
    for ( unsigned int i = 0; i < length; i++ )
        str += static_cast<char>( tolower(buffer[i]) );
    return str;
}

inline CString CString::LTrim() const {
    // Count spaces at the beginning
    unsigned int i = 0;
    while ( i < length && isspace(buffer[i]) ) i++;

    // Return trimmed string
    return Right(length - i);
}

inline CString CString::RTrim() const {
    // Look for first non space on the right
    int i = length - 1;
    int pos = -1;
    while ( i > 0 && pos == -1 ) {
        if ( !isspace(buffer[i]) ) pos = i+1;
        i--;
    }

    if ( pos == -1 ) pos = length;

    // Return trimmed string
    return Left(pos);
}

inline CString CString::Trim() const {
    return LTrim().RTrim();
}

inline CString CString::LSet(int len, const CString& c) const {
    CString str;
    for ( unsigned int i = 0; i < len - length; i++ )
        str += c.buffer[0];
    str += Left(len);
    return str;
}

inline CString CString::RSet(int len, const CString& c) const {
    CString str = Left(len);
    for ( int i = length; i < len; i++)
        str += c.buffer[0];
    return str;
}

inline CString CString::StripExt() const {
    // Find last "."
    const char* dotp = strrchr(buffer, '.');

    // If it contains no extension, return unmodified filename
    if ( dotp == NULL ) return *this;

    // Copy characters before the last "."
    CString str;
    const char* p = buffer;
    while ( p != dotp ) {
        str += *p;
        p++;
    }

    return str;
}

inline CString CString::StripDir() const {
    // Replace "\" with "/"
    CString filename = Replace("\\", "/");

    // Find last "/"
    const char* slashp = strrchr(filename.buffer, '/');

    // If contains no path, return unmodified filename
    if ( slashp == NULL ) return *this;

    // Copy all characters after the last "/"
    CString str;
    slashp++;   // Skip "/"
    while ( *slashp != '\0' ) {
        str += *slashp;
        slashp++;
    }

    return str;
}

inline CString CString::ExtractExt() const {
    // Find last "."
    const char* dotp = strrchr(buffer, '.');

    // If it contains no extension, return empty string
    if ( dotp == NULL ) return "";

    // Copy characters after the last "."
    CString str;
    dotp++; // Skip "."
    while ( *dotp != '\0' ) {
        str += *dotp;
        dotp++;
    }

    return str;
}

inline CString CString::ExtractDir() const {
    // Replace "\" with "/"
    CString filename = Replace("\\", "/");

    // Find last "/"
    const char* slashp = strrchr(filename.buffer, '/');

    // If contains no path, return empty string
    if ( slashp == NULL ) return "";

    // Copy all characters before the last "/"
    CString str = "";
    const char* p = filename.buffer;
    while ( p != slashp ) {
        str += *p;
        p++;
    }

    return str;
}

inline CString CString::RealPath() const {
    char path[FILENAME_MAX];
    realpath(buffer, path);
    return CString(path);
}

inline CString CString::Read(const CString& filename) {
    // Open file to read
    FILE* f = NULL;
    f = fopen(filename.buffer, "rb");

    // If it does not exist, return empty string
    if ( f == NULL ) return "";

    // Get file size
    fseek(f, 0, SEEK_END);
    long size = ftell(f);
    fseek(f, 0, SEEK_SET);

    // Allocate contents in a buffer
    char* buf = (char*)malloc(size+1);
    fread(buf, sizeof(char), size, f);
    buf[size] = '\0';

    // Copy buffer into string
    CString str = buf;

    // Free buffer
    free(buf);

    // Return string
    return str;
}

inline void CString::Write(const CString& filename, bool append) const {
    // Open file to write or append
    FILE* f = fopen(filename.buffer, append ? "ab" : "wb");

    // If it could not be opened, return
    if ( f == NULL ) return;

    // Write string buffer
    fwrite(buffer, sizeof(char), length, f);

    // Close file
    fclose(f);
}

inline Array<CString> CString::Split(const CString& delim) const {
    Array<CString> arr;

    // Return empty array if length = 0 or delim = 0
    if ( Length() == 0 || delim.buffer[0] == 0) return arr;

    // Fill array
    CString str;
    for ( unsigned int i = 0; i < Length(); i++ ) {
        char c = buffer[i];
        if ( c == delim.buffer[0] ) {
            arr.Add(str);
            str = "";
        } else {
            str += c;
        }
    }
    arr.Add(str);

    return arr;
}

#endif 
