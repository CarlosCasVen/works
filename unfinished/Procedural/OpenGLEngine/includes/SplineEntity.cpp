#include "../includes/SplineEntity.h"
#include "../includes/Renderer.h"
#include "../includes/DataBase.h"
#include "../includes/Vertex.h"
#include "../includes/Array.h"
#include <stddef.h>

#define VERTEX_SHADER_PATH "data/vshader.txt"
#define FRAGMENT_SHADER_PATH "data/fshader.txt"

SplineEntity::SplineEntity(Spline* spline) : Entity()
{
    m_spline = spline;
	m_shader = new Shader(VERTEX_SHADER_PATH, FRAGMENT_SHADER_PATH);
}

SplineEntity::~SplineEntity()
{

}

bool SplineEntity::Init()
{
    Array<Vertex> dataMesh;
    Array<unsigned int> indexes;

    Renderer& render = Renderer::Instance();

	Array<Vector3> curve = m_spline->GenerateSamples(5);

    for ( unsigned int index = 0; index < curve.Size(); index++ )
    {
        Vector3 normal = Vector3( 0, 0, 0 );
        normal = normal.Normalized();

        dataMesh.Add(
            Vertex(
                 curve[index],
                 0.0f,
                 0.0f,
                 normal
            )
         );
        indexes.Add( index );
    }


    m_vao = render.CreateVAO();
    m_vbo = render.CreateVBO();
    m_ebo = render.CreateVBO();

    render.BindVAO( m_vao );
    render.BindVBO( m_vbo, GL_ARRAY_BUFFER );

    glBufferData( GL_ARRAY_BUFFER, sizeof( Vertex ) * dataMesh.Size(), &dataMesh[ 0 ], GL_STATIC_DRAW );

    //enlace de los vertices
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, sizeof( Vertex ), ( const void* ) offsetof( Vertex, m_position ) );
    glEnableVertexAttribArray( 0 );

    //enlace de las normales
    glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, sizeof( Vertex ), ( const void* ) offsetof( Vertex, m_normal ) );
    glEnableVertexAttribArray( 1 );

    //enlace de las texcoords
    glVertexAttribPointer( 2, 2, GL_FLOAT, GL_FALSE, sizeof( Vertex ), ( const void* ) offsetof( Vertex, m_u ) );
    glEnableVertexAttribArray( 2 );

    render.BindVBO( m_ebo, GL_ELEMENT_ARRAY_BUFFER );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( unsigned int ) * indexes.Size(), &indexes[ 0 ], GL_STATIC_DRAW );

    render.BindVBO( 0, GL_ELEMENT_ARRAY_BUFFER );
    render.BindVBO( 0, GL_ARRAY_BUFFER );
    render.BindVAO( 0 );

    m_nIndices = ( uint16 ) indexes.Size();

    printf( "VALORES GUARDADOS DE LOS INDICES\n\n" );
    for( unsigned int index = 0; index < m_nIndices; index++ )
    {
        printf( "%d, ", indexes[ index ] );

        if( ( index + 1 ) % 3 == 0 ) printf( "\n" );
    }


    return true;
}

void SplineEntity::End()
{

}

uint32 SplineEntity::GetVAO()
{
    return m_vao;
}

uint32 SplineEntity::GetEBO()
{
    return m_ebo;
}

uint32 SplineEntity::GetNumIndices()
{
    return m_nIndices;
}

void SplineEntity::ActiveDiffuse()
{
}

void SplineEntity::Render()
{
	m_shader->ActiveProgram();

	Matrix4 projection = DataBase::Instance()->GetCamera()->GetProjection();
	Matrix4 view = DataBase::Instance()->GetCamera()->GetView();


	Renderer::Instance().ActivateTexture(0, m_textureDiffuse, 0);
	Matrix4 model = GetMoldeMatrix();
	Matrix4 mvp = projection * view * model;

	Renderer::Instance().SetUniformMatrix(0, model);
	Renderer::Instance().SetUniformMatrix(1, mvp);
	Renderer::Instance().SetUniformInt(8, 16);

	Renderer::Instance().BindVAO(m_vao);
	Renderer::Instance().BindVBO(m_ebo, GL_ELEMENT_ARRAY_BUFFER);

	glDrawElements(GL_LINE_STRIP, m_nIndices, GL_UNSIGNED_INT, 0);

	Renderer::Instance().BindVAO(0);
	Renderer::Instance().BindVBO(0, GL_ELEMENT_ARRAY_BUFFER);

	Renderer::Instance().BindTexture2D(0);


	m_shader->DescActiveProgram();
}