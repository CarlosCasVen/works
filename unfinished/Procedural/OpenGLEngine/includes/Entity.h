#ifndef ENTITY_H
#define ENTITY_H

#include "Array.h"
#include "Quat.h"
#include "Vector3.h"
#include "Matrix4.h"
#include "glew.h"
#include "freeglut.h"

class Entity {
public:
    Entity();
    ~Entity();

    virtual bool Init() = 0;
    virtual void End () = 0;

    virtual const Vector3& GetPosition() const;
    virtual Vector3& GetPosition();

    virtual const Quat& GetRotation() const;
    virtual Quat& GetRotation();

    virtual const Vector3& GetScale() const;
    virtual Vector3& GetScale();

    virtual void Move( const Vector3& speed );

    virtual void Update( float elapsed );

    virtual void Render();

    virtual void SetPosition( Vector3 pos );

    virtual Matrix4 GetMoldeMatrix();

    virtual void SetTypeOfDrawing( int type );


protected:
    Vector3 m_position;
    Quat    m_rotation;
    Vector3 m_scale;
    int     m_typeOfDraw;

};

#endif