#ifndef SCREEN_H
#define SCREEN_H

#include "Types.h"
#include "IScreen.h"


class Screen : public IScreen
{
public:
    virtual bool Init();
    virtual void End ();

    //Set options
    virtual void SetFullscreen( bool fullscreen           );
    virtual void SetWindowName( const char* name          );
    virtual void SetWindowSize( int16 width, int16 height );

    //Get Options
    virtual float GetElapsedTime(                               ) const;
    virtual void  GetWindowSize ( uint16* width, uint16* height ) const;

    virtual void Update( float elapsed );

    virtual void Clear( uint8 r, uint8 g, uint8 b, uint8 a = 255 );
    
private:
    Screen ();
    ~Screen();

    bool            m_isOpened;
    int32           m_milisecondsFromInit;
    float           m_elapsedTime;

    friend class IScreen;
};

#endif