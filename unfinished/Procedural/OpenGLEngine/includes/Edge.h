#ifndef EDGE_H
#define EDGE_H

#include "Entity.h"
#include "Shader.h"
#include "Spline.h"

class Edge : public Entity
{
public:
    Edge( Vector3 dir, Vector3 color, float distance = 100.0f );
    ~Edge();

    virtual bool Init();
    virtual void End();

    virtual void Render();

private:
    Shader* m_shader;

    uint32  m_vao;
    uint32  m_vbo;

    float   m_distance;
    Vector3 m_color;
    Vector3 m_dir;
};



#endif 