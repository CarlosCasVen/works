#ifndef CELL_H
#define CELL_H

#include "Entity.h"
#include "Shader.h"

class Cell : public Entity
{
public:
    Cell( const char* model, Vector3 color );
    ~Cell();

    virtual bool Init();
    virtual void End();

    virtual void Render();

private:
    Shader* m_shader;

    uint32  m_vao;
    uint32  m_vbo;
    uint32  m_ebo;
    uint32  m_nIndexes;

    Vector3     m_color;
    const char* m_path;

};



#endif 